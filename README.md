## Interactivelets Plugins

Coursera Plugin Adaptation for Interactive Puzzles https://gitlab.com/dainiak/interactivelets

---

1. Install packages:

```bash
npm install
```

2. Build plugin bundles:

```bash
npx webpack --config webpack.config.js
```

This will create a directory named `dist` with all the puzzles in it.

Auto rebuild:

```bash
npm run watch
```

3. To make changes to all manifest.json files at once, edit `src/static/manifests/manifest.template.json` and execute:

```bash
python manifestBuilder.py
```

4. Install Python 3, run sever:

```bash
FLASK_APP=main.py flask run
```

5. Open sandbox: http://courseraplugins.com/plugin-simulator, paste target plugin link  
   (e.g. `http://127.0.0.1:5000/dist/arthurs-books/arthurs-books-level1/`)  
   P.S. Don't forget the `/` in the end.

6. Archive target plugin for upload. For multi-level puzzles:

```bash
cd dist/[puzzle-name]
for file in ./*; do echo ${file}; zip -r ${file}.zip ${file}; done
```

The archive will appear in `dist/[puzzle-name]`.

For single-level puzzles:

```bash
cd dist
zip -r [puzzle name].zip [puzzle name]
```

The archive will appear in `dist`.

To archive all puzzles at once:

```bash
bash archivePuzzles.sh
```

### File structure

```
    - src
        - legacy
            - html #old puzzles html
            - js #old puzzles js
            - static #old puzzles css and images
        - next #new puzzles (React + Redux)
            -clientOnly #versions for discrete-math-puzzles.github.io
```

### Configurations

Each multiple level puzzle has the following configuration:

```
{level: <number from 0 to N>}
```

The exact number of levels is

Course #2  
road-repair +  
balanced-graphs +    
bridges +  
find-number-2 +   
graph-cliques +  
guarinis-puzzle +    
hamiltonian-cycle +  
job-assignment +  
largest-number +      
make-a-tree +    
map-coloring +    
number-of-paths +   
subway-lines +    
summing-up-binomials +       
take-the-last-rock +   
touch-all-segments +    
maximum-advertisement-revenue +    
primitive-calculator

Course #1:
15 Game (with progress saving)
Antimagic Square (with progress saving)
Antivirus System (with progress saving)
Arthurs Books (with progress saving)
Arthurs Books Solver (with progress saving)
Balls In Boxes (with progress saving)
Chessboard Tiling v.4 Clock Game (with progress saving)
Cut The Figure (with progress saving)
Florin Coins (with progress saving)
Hanoi Towers (with progress saving)
Local Maximum  (with progress saving)
Magic Square (with progress saving)
N Bishops (with progress saving)
N Diagonals (with progress saving)
N Knights (with progress saving)
N Queens N Rooks (with progress saving)
Numbers In Boxes (with progress saving)
Numbers On Chessboard (with progress saving)
Opposite Colors (with progress saving)
Piece On Chessboard (with progress saving)
Prime (with progress saving)
Regular Graph (with progress saving)
Split Weights (with progress saving)
Summing Up Digits (with progress saving)
Sums Of Rows And Columns (with progress saving)
Switching Signs (with progress saving)
Three Hotels (with progress saving)
Tossing Coins (with progress saving)
Twice Numbers Puzzle
