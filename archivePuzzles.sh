for puzzleName in dist/*; do
    echo ${puzzleName}
    sublevels=$(find "$puzzleName" -type d -depth 1)
    if [[ -z "$sublevels" ]]; then
        echo "single level puzzle: $puzzleName"
        zip -r "$puzzleName.zip" "$puzzleName"
    else
        echo "multilevel puzzle: $puzzleName"
        for file in $puzzleName/*; do echo ${file}; zip -r ${file}.zip ${file}; done
    fi
done
