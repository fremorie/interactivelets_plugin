module.exports = function (api) {
    api.cache(true);
    const presets = [
        "@babel/preset-env",
        "@babel/preset-flow",
        "@babel/preset-react",
    ];
    const plugins = [
        "@babel/plugin-proposal-optional-chaining",
        "transform-object-rest-spread",
        "@babel/plugin-transform-runtime",
        "@babel/plugin-proposal-class-properties",
    ];
    return {
        presets,
        plugins,
    };
};
