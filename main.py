import flask
import os
from flask_cors import CORS

app = flask.Flask(__name__)
CORS(app)

CONTENT_TYPES = {
  'svg' : 'image/svg+xml',
  'js' : 'application/javascript',
  'css' : 'text/css'
}

def get(path):
  with open(path, 'rb') as f:
    response = flask.Response(response=f.read())
#     response.headers.add('Access-Control-Allow-Origin', 'http://courseraplugins.com')
    response.headers.add('Access-Control-Allow-Origin', '*')

    for k in CONTENT_TYPES:
      if path.endswith(k):
        response.headers.add('Content-Type', CONTENT_TYPES[k])

    return response

@app.route('/<path:path>', methods=['GET'])
def r1(path):
  if os.path.isdir(path):
    path = os.path.join(path, 'index.html')

    app.logger.info('  -> %s' % (path,))

  print('looking for %s: %s' % (path, os.path.exists(path)))

  try:
    return get(path)
  except FileNotFoundError:
    return ('', 404)
