from string import Template
import os

def prettify_puzzle_name(ugly_name):
    return ' '.join([
       word[0].upper() + word[1:]
       for word in ugly_name.split('-')
   ])  + ' (with progress saving)'

def get_prefix(name):
    return ''.join([
        word[0].upper() + word[1:]
        for word in name.split('-')
    ])

def main():
    root = 'src/legacy/static/manifests/'
    template_path = os.path.join(root, 'manifest.template.json')

    with open(template_path, 'r') as f:
        template = Template(f.read())

    for puzzle_name in os.listdir(root):
        puzzle_path = os.path.join(root, puzzle_name)
        if not os.path.isdir(puzzle_path):
            continue
        sub_puzzles = [item for item in os.listdir(puzzle_path) if os.path.isdir(os.path.join(puzzle_path, item))]
        if len(sub_puzzles) == 0:
            name = prettify_puzzle_name(puzzle_name)
            prefix = get_prefix(puzzle_name)
            manifest = template.substitute(name=name, shortname=name, prefix=prefix)
            with open(os.path.join(puzzle_path, 'manifest.json'), 'w') as f:
                f.write(manifest)
        else:
            for sub_puzzle_name in sub_puzzles:
                sub_puzzle_path = os.path.join(puzzle_path, sub_puzzle_name)
                name = prettify_puzzle_name(sub_puzzle_name)
                prefix = get_prefix(sub_puzzle_name)
                manifest = template.substitute(name=name, shortname=name, prefix=prefix)
                with open(os.path.join(sub_puzzle_path, 'manifest.json'), 'w') as f:
                    f.write(manifest)

    print('All done! Today is a gooood day.')


if __name__ == '__main__':
    main()
