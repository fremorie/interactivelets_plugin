import {LEVELS} from './constants';
import computer from '../../../static/images/computer.png';

export default function (level) {
    const $modal = $('.antivirus-system__modal');

    const AntivirusSystem = function (targetName, index, data, cb) {
        this.data = data || {
            circles: {},
            vertices: [],
            success: []
        };

        this.initVars();

        this.COMPUTER_SIZE = 30;
        this.MAX_DIMENSION = this.points.reduce(function (max, current) {
            if (max < this.circles[current][0]) {
                max = this.circles[current][0];
            }
            if (max < this.circles[current][1]) {
                max = this.circles[current][1];
            }
            return max;
        }.bind(this), -1);

        this._cb = cb || function () {
        };
        this._targetName = targetName;
        this._id = index;

        this.snap = Snap(targetName).attr({
            viewBox: '0 0 ' + (this.MAX_DIMENSION + 2 * this.COMPUTER_SIZE) + ' ' + (this.MAX_DIMENSION)
        });

        return this;
    };

    AntivirusSystem.prototype.initVars = function () {
        this._nodes = [];
        this._vertices = [];

        this.circles = this.data.circles;
        this.points = Object.keys(this.circles);

        this.vertices = this.data.vertices;
        this.success = this.data.success;

        this.path = [];

        return this;
    };

    AntivirusSystem.prototype.init = function () {
        this.initVars();
        this.draw();

        return this;
    };

    AntivirusSystem.prototype.draw = function () {
        this.drawVertices();
        this.drawCircles();

        return this;
    };

    AntivirusSystem.prototype.drawCircles = function () {
        if (this.circleGroup) {
            this.circleGroup.remove();
        }

        if (this.backGroup) {
            this.backGroup.remove();
        }

        this.backGroup = this.snap.g().attr({ id: 'back' });
        this.circleGroup = this.snap.g().attr({ id: 'circles' });

        this.points.forEach(function (point, i) {
            const coords = this.circles[point];

            const back = this.snap
                .circle(coords[0], coords[1], this.COMPUTER_SIZE / 2 + 2)
                .attr({
                    fill: '#fff',
                    stroke: this.path.indexOf(point) !== -1 ? '#000' : '#fff',
                    'stroke-width': '2',
                });

            const circle = this.snap
                .image(
                    'computer.png',
                    coords[0] - this.COMPUTER_SIZE / 2,
                    coords[1] - this.COMPUTER_SIZE / 2,
                    this.COMPUTER_SIZE, this.COMPUTER_SIZE
                )
                .attr({'data-id': point, class: 'circle'})
                .mousedown(this.handleCircleClick.bind(this));

            this._nodes[i] = circle;
            this.circleGroup.add(circle);
            this.backGroup.add(back);
        }.bind(this));

        return this;
    };

    AntivirusSystem.prototype.handleCircleClick = function (e) {
        e.preventDefault;

        const id = e.target.getAttribute('data-id');
        const index = this.path.indexOf(id);

        if (index !== -1) {
            this.path = [].concat(this.path.slice(0, index), this.path.slice(index + 1));
        } else {
            this.path.push(id);
        }

        this.draw();

        this.checkValid();
    } ;

    AntivirusSystem.prototype.drawVertices = function () {
        if (this.verticesGroup) {
            this.verticesGroup.remove();
        }

        if (this.vertices.length) {
            this.verticesGroup = this.snap.g().attr({ id: 'vertices' });

            this.vertices.forEach(function (p, i) {
                const A = p[0];
                const B = p[1];
                const coordsA = this.circles[A];
                const coordsB = this.circles[B];

                this.verticesGroup.add(this.snap
                    .path('M' + [coordsA[0], coordsA[1], coordsB[0], coordsB[1]].toString())
                    .attr({
                        'data-id': p,
                        stroke: '#000',
                        'stroke-width': '2',
                        fill: 'none'
                    })
                );
            }.bind(this));
        }

        return this;
    };

    AntivirusSystem.prototype.handleVerticeClick = function (e) {
        e.preventDefault();

        const id = e.target.getAttribute('data-id');
        const index = this.path.indexOf(id);

        if (index !== -1) {
            this.path = [].concat(this.path.slice(0, index), this.path.slice(index + 1));
        } else {
            this.path.push(id);
        }

        this.draw();
    };

    AntivirusSystem.prototype.checkValid = function (e) {
        const sortedPath = this.path.sort().join('');
        let currentVertices = this.vertices.concat();

        this.path.forEach(function (point) {
            currentVertices = currentVertices.map(function (vertex) {
                if (vertex.indexOf(point) === -1) {
                    return vertex;
                }

                return false;
            }).filter(Boolean);
        });

        if (currentVertices.length) {
            // $modal.find('.modal-body h4').text(errors.empty);
            // $modal.modal('show');

            currentVertices.forEach(function (vertex) {
                $(this._targetName + ' path[data-id=' + vertex + ']').attr('stroke', '#f00');
            }, this);
        } else {
            if (this.path.length === this.success) {
                this._cb();
            } else {
                $modal.find('.modal-body h4').text(errors.cheaper);
                $modal.modal('show');
            }
        }
    };

    var errors = {
        empty: 'Some network connection is not covered.',
        cheaper: 'Try to find a smaller set of computers!'
    };

    const graph = new AntivirusSystem(
            '#graph',
            level,
            LEVELS[level],
            function() {
                window.q.successCb();
            }).init();

    $('.reset').on('click', function() {
        graph.init();
    });

    $('.check').on('click', function() {
        graph.checkValid();
    });

    $('.impossible').on('click', function() {
        $('#possible_modal').modal('show');
    });
};
