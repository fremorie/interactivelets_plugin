import manifest from '../../../static/manifests/antivirus-system/manifest.json';
import puzzle from './app';
import puzzleWrapper from '../../puzzleWrapper';

puzzleWrapper({puzzle, height: 720});
