export const LEVELS = {
    0: {
        circles: {
            A: [30, 300],
            B: [180, 30],
            C: [330, 300],
            D: [180, 180],
            E: [180, 120],
            F: [130, 235],
            G: [230, 235]
        },
        vertices: ['AB', 'AE', 'AG', 'AC', 'CF', 'CE', 'CB', 'FD', 'FB', 'DE', 'DG', 'GB'],
        success: 4
    },
    1: {
        circles: {
            A: [30, 180],
            B: [120, 30],
            C: [120, 330],
            D: [280, 180],
            E: [350, 30],
            F: [350, 330],
            G: [430, 180],
        },
        vertices: ['BC', 'EF', 'EG', 'DF', 'AB', 'AC', 'AD', 'DG', 'CE'],
        success: 4
    },
    2: {
        circles: {
            A: [547, 218],
            B: [454, 509],
            C: [148, 511],
            D: [52, 221],
            E: [298, 40],
            F: [414, 262],
            G: [371, 397],
            H: [230, 397],
            I: [185, 263],
            J: [299, 180]
        },
        vertices: ['DC', 'CB', 'BA', 'AE', 'DE', 'CH', 'DI', 'EJ', 'AF', 'BG', 'JH', 'JG', 'IF', 'IG', 'FH'],
        success: 6
    }
};
