import core from '../../core';
import initCoursera from '../../init';

const LEVELS = [1, 2];
const STATES = [true, false];

function checkMatchingAnswer(firstSelect, secondSelect) {
    if( $(firstSelect).attr('data-answer') == $(secondSelect).attr('data-answer')) {
        $(firstSelect).addClass('empty');
        $(secondSelect).addClass('correct');

        $(firstSelect).draggable({
            revert: false,
            disabled: true
        });

        $(secondSelect).droppable({
            disabled: true
        });

        $(firstSelect).removeClass('selected').addClass('disabled');
        firstSelect = undefined;
    } else {
        $(secondSelect).addClass('incorrect').unbind('click');

        $(firstSelect).removeClass('selected');
        $(secondSelect).removeClass('incorrect');
        firstSelect = undefined;
    }
}

$(document).ready(function(){
    initCoursera({height: 750});
    core({});

    $('.apple').draggable({
        stack: '.apple',
        snap: '.target',
        snapMode: 'inner',
        snapTolerance: 20,
        helper: 'clone',
        zIndex: 99,
        refreshPositions: true,
        revert: true,
    }).click(function(){
        const firstSelect = $(this);
        $(firstSelect).addClass('selected');
        $('.target').bind('click');
        $('.target').click(function(){
            const secondSelect = $(this);
            $('.target').unbind('click');
            checkMatchingAnswer(firstSelect, secondSelect);
       });
    });

    $('.target').droppable({
        hoverClass: 'drop-hover',
        drop: function( event, ui ) {
            if ($('#level1.active').length) {
                const filledTargets = $('.target.ui-droppable-disabled');

                if (filledTargets.length === 15) {
                    const matrix = {};

                    filledTargets.each(function(i, el) {
                        const xy = String($(el).data('xy'));

                        if (matrix[xy[0]]) {
                            matrix[xy[0]].push(xy[1]);
                        } else {
                            matrix[xy[0]] = [xy[1]];
                        }
                    })

                    const matrixKeys = Object.keys(matrix);
                    const matrixLength = matrixKeys.length;
                    let isValid = true;

                    if (matrixLength === 3 || matrixLength === 5) {
                        let i = 0;
                        const l = matrixLength;
                        for(; i < l; i++) {
                            const rl = matrix[matrixKeys[i]].length;

                            if ((matrixLength === 3 && rl === 5) || (matrixLength === 5 && rl === 3)) {
                                for(let j = 0; j < (rl - 1); j++) {
                                    if (Math.abs(matrix[matrixKeys[i]][j] - matrix[matrixKeys[i]][j + 1]) !== 1) {
                                        isValid = false;
                                    }
                                }
                            } else {
                                isValid = false;
                            }
                        }
                    } else {
                        isValid = false;
                    }

                    if (isValid) {
                        window.q.successCb(0, LEVELS);
                    } else {
                        $('#possible_modal').modal('show');
                    }
                }
            }
          }
    });

    $('.apple').on('dragstart', function () {
        $('.card').addClass('dragstart');
        $(this).addClass('empty');
        setTimeout(function() {
            $('.card').addClass('dragging');
        }, 100 );
    });

    $('.apple').on('dragstop', function(){
        $(this).removeClass('empty');
        $('.card').removeClass('dragging').removeClass('dragstart');
    });

    $('.target').on('drop', function(event, ui){
        const firstSelect = $(ui.draggable);
        const secondSelect = $(this);
        checkMatchingAnswer(firstSelect, secondSelect);
    });

    $('.reset').on('click', function (e) {
        $('.tab-pane.active .target.correct.ui-droppable-disabled').removeClass('correct').removeClass('ui-droppable-disabled').droppable({
            disabled: false
        });
        $('.tab-pane.active .apple.ui-draggable-disabled.disabled').removeClass('disabled').removeClass('ui-draggable-disabled').draggable({
            revert: true,
            disabled: false
        });
    });

    $('.impossible').on('click', function (e) {
        if (e.currentTarget) {
            const level = $(e.currentTarget).data('id');

            if (!STATES[level - 1]) {

                window.q.successCb(level - 1, LEVELS);
            } else {
                $('#possible_modal').modal('show');
            }
        }
    });
});
