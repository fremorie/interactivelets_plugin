import {MAX_DAYS} from './constants';

function BookShelfSolver(shelfSelector, cb) {
    this.container = $(shelfSelector);
    this.books = this.container.find('.arturs-books__book');
    this.displayStep = this.container.find('.arturs-books__steps-step-description');
    this.displayStart = this.container.find('.arturs-books__steps-start-description');
    this.displayRecord = $('.arturs-books__score-value');
    this.autoStepInterval = 50e3;
    this.step = 1;
    this.defaultDisplayText = 'Press solve to see the solution';
    this._cb = cb;
    return this;
}

BookShelfSolver.prototype.init = function () {
    this.step = 1;
    this.displayStart.text(this.defaultDisplayText);
    this.displayStep.text('');
    this.displayRecord.text(this.getRecord());
    this.setModalStatus();
    return this;
};

BookShelfSolver.prototype.block = function () {
    this.books.find('.arturs-books__number input').prop('disabled', true);

    return this;
};

BookShelfSolver.prototype.active = function (i) {
    $(this.books[i]).addClass('active');

    return this;
};

BookShelfSolver.prototype.unactive = function () {
    this.books.removeClass('active');

    return this;
};

BookShelfSolver.prototype.unblock = function () {
    this.books.find('.arturs-books__number input').prop('disabled', false);

    return this;
};

BookShelfSolver.prototype.isSolved = function () {
    return this.getSwapPair() === null;
};

BookShelfSolver.prototype.getSwapPair = function checkOrder() {
    const values = this.getValues();

    for (let i = 0; i < values.length; i++) {
        const value = values[i];

        if (value != i + 1) {
            return [value - 1, i];
        }
    }

    return null;
};

BookShelfSolver.prototype.getValues = function () {
    return this.books.find('.arturs-books__number input').map(function (i, input) {
        return ~~input.value;
    });
};

BookShelfSolver.prototype.isValide = function () {
    const values = this.getValues();
    const used = {};

    for (let i = 0; i < values.length; i++) {
        const value = values[i];

        if (value > 10 || value <= 0 || used[value]) {
            return false;
        }
        used[value] = true;
    }

    return true;
};

BookShelfSolver.prototype.solve = function () {
    if (this.isSolved()) {
        return this;
    }

    this.block();
    this.displayStart.text('Press solve to see the next step');
    const swapPair = this.getSwapPair();

    this.active(swapPair[0]);
    this.active(swapPair[1]);

    if (this.timoutId) {
        this.solveStep2();
        return this;
    }

    this.displayStep.text('On day ' + this.step + ' we place volume ' + (swapPair[0] + 1) + ' on its place')
    this.timoutId = setTimeout(this.solveStep2.bind(this), this.autoStepInterval);

    return this;
};

BookShelfSolver.prototype.solveStep2 = function () {
    const swapPair = this.getSwapPair();

    this.timoutId = clearTimeout(this.timoutId);
    this.unactive();
    this.swap(swapPair);
    this.step++;

    if (!this.isSolved()) {
        this.solve();
    } else {
        this.displayStart.text('Press reset to start all over again');
        this.setRecord(this.step - 1);
        this.displayRecord.text(this.step - 1);
        this.setModalStatus();
        const record = this.getRecord() || this.step - 1;

        if (record == MAX_DAYS) {
            this.impossibleModal();
            this._cb();
        }
    }

    return this;
};

BookShelfSolver.prototype.setRecord = function (record) {
    const prevRecord = this.getRecord();
    window.q.setCookie('arturs-books-score', Math.max(record, prevRecord));
    return Math.max(record, prevRecord);
};


BookShelfSolver.prototype.getRecord = function () {
    return window.q.getCookie('arturs-books-score') || 0;
};

BookShelfSolver.prototype.setModalStatus = function () {
    const record = this.getRecord();
    if (record == MAX_DAYS) {
        this.impossibleModal();
    } else {
        this.possibleModal()
    }
};

BookShelfSolver.prototype.possibleModal = function () {
    $('.modal-button').attr('id', 'possible');
    $('.modal-button').data('data-target', '#possible_modal');
};

BookShelfSolver.prototype.impossibleModal = function () {
    $('.modal-button').attr('id', 'impossible');
    $('.modal-button').attr('data-target', '#congratulations_modal');
};

BookShelfSolver.prototype.swap = function (pair) {
    const bookA = this.books[pair[0]];
    const bookB = this.books[pair[1]];
    const bookANumber = this.getBookNumber(bookA);
    const bookBNumber = this.getBookNumber(bookB);

    this.setBookNumber(bookA, bookBNumber);
    this.setBookNumber(bookB, bookANumber);
};

BookShelfSolver.prototype.getBookNumber = function (book) {
    return $(book).find('.arturs-books__number input').val();
};

BookShelfSolver.prototype.setBookNumber = function (book, number) {
    return $(book).find('.arturs-books__number input').val(number);
};

BookShelfSolver.prototype.flush = function () {
    const books = this.books;

    for (let i = 0; i < books.length; i++) {
        const book = books[i];

        $(book).find('.arturs-books__number input').val('');
    }
    this.unactive();
    this.unblock();
    this.timoutId = clearTimeout(this.timoutId);

    return this;
};

export default BookShelfSolver;
