import core from '../../core';
import initCoursera from '../../init';
import BookShelfSolver from "./bookShelfSolver";

import puzzleWrapper from '../../puzzleWrapper';

const game = () => {
    const bookSelfSolver = new BookShelfSolver('#level5 .arturs-books__container', function () {
        window.q.successCb();
    }).init();

    $('.reset').on('click', function () {
        var id = $(this).data('id') - 1;

        if (id == 5) {
            bookSelfSolver.flush().init();
        }
    });

    $('.solve').on('click', function () {
        var id = $(this).data('id') - 1;

        if (id == 5) {
            if (bookSelfSolver.isValide()) {
                bookSelfSolver.solve();
            } else {
                bookSelfSolver.displayStep.text('Your input is invalid. Try to correct');
            }
        }
    });

    $('#congratulations_modal').on('show.bs.modal', function (e) {
        if (e.relatedTarget) {
            window.q.successCb();
        }
    });
};

puzzleWrapper({puzzle: game, height: 500});
