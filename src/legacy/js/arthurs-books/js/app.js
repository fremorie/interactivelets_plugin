import {LEVELS} from './constants';

export default function (level) {
    function BookShelf(order, days, cb) {
        this.container = $('#ArthursBooksGame');
        this.books = this.container.find('.arturs-books__book');
        this.books.click(this.onclick.bind(this));
        this.order = order;
        this.limit = days;
        this.swap = null;
        this.steps = 0;
        this._cb = cb;

        return this;
    }

    BookShelf.prototype.unselect = function unswap() {
        this.swap.removeClass('active');
        this.swap = null;
    };

    BookShelf.prototype.select = function (el) {
        el.addClass('active');
        this.swap = el;
    };

    BookShelf.prototype.init = function () {
        var books = this.books;
        this.setDays(0);
        this.container.find('.arturs-books__steps-number').text(this.steps + '');

        for (var i = 0; i < books.length; i++) {
            var book = books[i];

            $(book).find('.arturs-books__number').text(this.order[i]);
        }

        return this;
    };

    BookShelf.prototype.setDays = function (steps) {
        this.steps = steps;
        this.container.find('.arturs-books__steps-number').text(this.steps + '');
    };

    BookShelf.prototype.onclick = function (e) {
        if (this.steps >= this.limit) {
            return;
        }

        var $this = $(e.currentTarget);

        if (this.swap) {
            var swapText = this.swap.find('.arturs-books__number');
            var swapNumber = swapText.text();
            var $thisText = $this.find('.arturs-books__number');
            var $thisNumber = $thisText.text();
            swapText.text($thisNumber);
            $thisText.text(swapNumber);
            if (e.currentTarget !== this.swap.get(0)) {
                this.setDays(this.steps + 1);
            }
            this.unselect();

            if (this.checkOrder(this.books) && this.steps <= this.limit) {
                this.steps = Infinity;
                this._cb();
            }
        } else {
            this.select($this);
        }
    };

    BookShelf.prototype.checkOrder = function checkOrder(books) {
        for (var i = 0; i < books.length; i++) {
            var book = books[i];

            if ($(book).find('.arturs-books__number').text() != i + 1) {
                return false;
            }
        }

        return true;
    };

    BookShelf.prototype.flush = function checkOrder() {
        if (this.swap) {
            this.unselect();
        }

        return this;
    };

    const levelInfo = LEVELS[level];
    const shelf = new BookShelf(
        levelInfo.order,
        levelInfo.limit,
        function () {
            window.q.successCb();
        }
    ).init();

    $('.reset').on('click', function () {
        shelf.flush().init();
    });
};
