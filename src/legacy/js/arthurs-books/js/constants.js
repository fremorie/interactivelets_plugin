export const LEVELS = [
    { order: [3, 7, 10, 5, 9, 1, 8, 4, 6, 2], limit: Infinity },
    { order: [10, 9, 8, 7, 6, 5, 4, 3, 2, 1], limit: 5 },
    { order: [3, 1, 2, 6, 4, 5, 10, 7, 8, 9], limit: 7 },
    { order: [5, 8, 7, 1, 9, 2, 10, 6, 4, 3], limit: 7 },
    { order: [6, 9, 5, 7, 10, 3, 2, 1, 8, 4], limit: 9 }
];

// max number of days needed to solve arthurs books puzzle (worst case, level 6)
export const MAX_DAYS = 9;
