export const LEVELS = {
    0: { circlesCount: 5, isPossible: true },
    1: { circlesCount: 6, isPossible: false }
};
