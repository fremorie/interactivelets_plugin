import balancedGraphs from './app';
import puzzleWrapper from "../../puzzleWrapper";

puzzleWrapper({puzzle: () => balancedGraphs(1), height: 779});
