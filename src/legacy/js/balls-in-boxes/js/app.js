export default function(level) {
    var Balls = function (targetName, id, successCount, cb) {
        this.inputs = $(targetName + ' input');
        this.result = $('.result');
        this.counters = [];

        this._id = id;
        this.successCount = successCount;

        this._cb = cb || function () {};

        this.inputs.on('keyup', this.onKeyupHandler.bind(this))
    };

    Balls.prototype.onKeyupHandler = function () {
        this.counters = [];

        this.inputs.removeClass('error');

        this.inputs.each(function(i, input) {
            if (input.value !== '') {
                var value = Number(input.value);
                var index = this.counters.indexOf(value);

                if (index !== -1) {
                    $(this.inputs[index]).addClass('error');
                    $(input).addClass('error')
                }
                if (value > this.successCount || value < 0) {
                    $(input).addClass('error')
                }

                this.counters.push(value);
            } else {
                this.counters.push(false);
            }
        }.bind(this));

        var filtered = this.counters.filter(function (item) { return item !== false });
        var sum = this.counters.reduce(function(r, i) { return r + Number(i); }, 0);

        this.result.html(sum);

        if (filtered.length !== this.counters.length) {
            return;
        }

        if (sum === this.successCount && !this.inputs.closest('.balls').find('input.error').length) {
            this._cb(this._id);
        }
    };

    var triggered = false;
    var data = [60, 45, 30];

    const ball = new Balls(
        '#balls',
        level,
        data[level],
        function () {
            window.q.successCb();
        }
    );

    $('.reset').on('click', function() {
        ball.inputs.each(function(i, input) {
           $(input).val('').removeClass('error');
        });
        ball.result.text('0');
    });

    $('#impossible').click(function (e) {
        e.preventDefault();
        if (e.target) {
            if (level === 2) {
                window.q.successCb();
            } else {
                $('#possible_modal').modal('show');
            }
        }
    });
};
