import './bridge1.svg';
import './bridge2.svg';
import  {PATHS_DATA, DESTINATION_RADIUS, DESTINATIONS} from './constants';

export default function(level) {

    const Bridge = function (targetName, id, cb) {
        this.snap = Snap(targetName).attr({viewBox: '0 0 544 646'});
        this.snap.image('bridge' + id + '.svg', 0, 0, 544, 646);
        this._id = id;
        this._cb = cb;

        this.flush();
    };

    Bridge.prototype.init = function () {
        this.current;
        this.path = [];

        this.data = PATHS_DATA[level] || {};
        this._g && this._g.remove();
        this._gp && this._gp.remove();
        this._gp = {};
        this._g = {};
        this._p = {};
        this._currentDestination = undefined;
        this.destinations = undefined;
        this._destinationsGroup = undefined;
    };

    Bridge.prototype.draw = function (cb) {
        this._addBridges();
        this._addPaths();
        this._addDestinations();

        return this;
    };

    Bridge.prototype._addDestinations = function () {
        this.destinations = {};
        this._destinationsGroup = this.snap.g().attr({ id: 'destinations' });

        Object.keys(DESTINATIONS).forEach(function (key) {
            const destination = DESTINATIONS[key];
            const circle = this.snap
                .circle(destination.x, destination.y, DESTINATION_RADIUS)
                .attr({
                    fill: '#fff',
                    stroke: '#2771FF',
                    'stroke-width': 2,
                    destinationId: key
                })
                .mousedown(function (event) {
                    event.preventDefault();

                    const destinationId = event.target.getAttribute('destinationId');

                    if (!this._currentDestination) {
                        this.destinations[destinationId].attr({fill: '#2771FF'});

                        this._currentDestination = destinationId;
                    }
                }.bind(this));

            const text = this.snap
                .text(destination.x - 11, destination.y + 12, key)
                .attr({'font-size': 35, 'fill': '#ccc'});

            this.destinations[key] = circle;
            this._destinationsGroup.add(circle, text);
        }.bind(this));

        return this;
    };

    Bridge.prototype._addBridges = function () {
        const bridges = this.data.bridges || {};

        this._b = {};

        Object.keys(bridges).forEach(function (key) {
            const data = bridges[key].data;
            const translate = bridges[key].translate;

            this._b[key] = this.snap
                .rect(data[0], data[1], data[2], data[3], data[4])
                .attr({
                    transform: translate,
                    targetId: key
                })
                .mousedown(function (event) {
                    event.preventDefault();

                    const targetId = event.target.getAttribute('targetId');
                    const destinations = bridges[key].destinations;

                    if (destinations.indexOf(this._currentDestination) !== -1 && (this.path && this.path.indexOf(targetId) === -1) || !this.path) {
                        this.path.push(targetId);
                        const previousDestination = this._currentDestination;
                        this._currentDestination = destinations[0] !== this._currentDestination ? destinations[0] : destinations[1];

                        // change colors of active position
                        this.destinations[previousDestination].attr({ fill: '#fff' });
                        this.destinations[this._currentDestination].attr({ fill: '#2771FF' });
                        this._b[targetId].attr({ fill: '#2771FF' });

                        // draw line
                        this.triggerRenderPath([previousDestination, this._currentDestination, targetId].join(''));

                        if (this.path.length === 6 && this.path.reduce(function(r, a) { return r + Number(a);}, 0) === 21 && this._id === 2) {
                            this._cb(this._id);
                        }
                    }
                }.bind(this))
        }.bind(this));

        this._g = this.snap.g().attr({
            transform: 'translate(137, 39)',
            stroke: '#2771FF',
            'stroke-width': '2',
            fill: '#40a738'
        });

        Object.keys(this._b).forEach(function (b) {
            this._g.add(this._b[b]);
        }.bind(this));
    };

    Bridge.prototype._addPaths = function () {
        const paths = this.data.paths || {};

        this._p = {};

        Object.keys(paths).forEach(function (key) {
            const data = paths[key] || {};

            if (data && data.to) {
                this._p[key] = this.snap.path('M0,0').attr({
                    targetPath: 'M' + data.to.toString(),
                    targetId: key
                });

                const reversedKey = key.concat().split('').splice(0, 2).reverse().join('') + key[2];

                this._p[reversedKey] = this.snap.path('M0,0').attr({
                    targetPath: 'M' + data.to.concat().reverse().toString(),
                    targetId: reversedKey
                });
            }
        }.bind(this));

        this._gp = this.snap.g().attr({
            stroke: '#2771FF',
            'stroke-width': '3',
            fill: 'none'
        });

        Object.keys(this._p).forEach(function (p) {
            this._gp.add(this._p[p]);
        }.bind(this));
    };

    Bridge.prototype.triggerRenderPath = function (p) {
        const path = this._p[p];

        if (!path.attr('state')) {
            Snap.animate(
                0,
                Snap.path.getTotalLength(path.attr('targetPath')),
                function (step) {
                    path.attr({
                        path: Snap.path.getSubpath(path.attr('targetPath'), 0, step)
                    });
                }.bind(this),
                2000,
                mina.easeInOut,
                function () {
                    path.attr({ state: 'finished' });
                }
            );
        }
    };

    Bridge.prototype.flush = function () {
        this.init();
        this.draw();

        return this;
    };

    const bridge = new Bridge(
        `#bridge${level}`,
        level,
        function () {
            window.q.successCb();
        });

    $('.reset').on('click', function() {
        bridge.flush();
    });

    $('#congratulations_modal').on('show.bs.modal', function (e) {
        if (e.relatedTarget) {
            if (level === 1) {
                window.q.successCb();
            }
        }
    });
};
