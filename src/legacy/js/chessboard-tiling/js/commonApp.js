import app from './app';
import core from '../../core';
import initCoursera from '../../init';
import css from './styles.css';

courseraApi.callMethod({
    type: 'GET_SESSION_CONFIGURATION',
    onSuccess: config => {
        const {level} = config;
        if (level > 0) {
            document.querySelectorAll('.demo-button').forEach(element => element.classList.add('hidden'));
        } else {
            document.querySelectorAll('.impossible').forEach(element => element.classList.add('hidden'));
        }
        core({level});
        initCoursera({height: 730, level});
        app(level);
    },
    onError: error => {
        console.error(`Failed to get session configuration, traceback: ${error}`)
    },
});
