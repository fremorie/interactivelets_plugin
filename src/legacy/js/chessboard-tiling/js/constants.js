export const SCHEMES = {
    0: {isPossible: true},
    1: {isPossible: false},
    2: {isPossible: true},
    3: {isPossible: false},
    4: {isPossible: true},
    5: {isPossible: false},
};
