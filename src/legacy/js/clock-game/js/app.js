import {LEVELS_DATA} from './constants';

export default function (level) {
    var $modal = $('.clock-game__modal');

    var Clock = function (targetName, k, id, cb) {
        this.targetName = targetName;
        this.id = id;
        this.k = k;
        this.N = Math.pow(2, k) - 1;
        this.count = 0;

        this.lower = 1;
        this.upper = this.N;

        this.$activeTab = $(this.targetName);
        this.$coutner = this.$activeTab.find('.history .counter');
        this.$history = this.$activeTab.find('.history .content');
        this.$input = this.$activeTab.find('.form-control');
        this.$ask = this.$activeTab.find('.ask');
        this.$reset = this.$activeTab.find('.reset');

        this._cb = cb;

        this.$ask.on('click', this.step.bind(this));
        this.$reset.on('click', this.flush.bind(this));

        this.$coutner.text('There are ' + this.k + ' questions left.');

        $modal.on('click', '.btn', this.flush.bind(this));
    }

    Clock.prototype.flush = function () {
        this.lower = 1;
        this.upper = this.N;
        this.m = null;
        this.count = 0;

        this.$history.addClass('gray').text('History is empty. Make your prediction.');
        this.$coutner.text('There are ' + this.k + ' questions left.');
        this.$input.removeAttr('disabled');
        this.$ask.removeAttr('disabled');
    }

    Clock.prototype.step = function () {
        var userSuggestion = this.$input.val();

        if (isNaN(Number(userSuggestion)) || !Number.isInteger(Number(userSuggestion))) {
            this.$input.addClass('error');
            return;
        } else {
            this.$input.removeClass('error');
        }

        userSuggestion = Number(userSuggestion);

        this.count++;
        this.$input.val('');

        if (this.$history.hasClass('gray')) {
            this.$history.removeClass('gray').html('');
        }

        this.$coutner.text('There are ' + (this.k - this.count <= 0 ? 'no' : (this.k - this.count)) + ' questions left.');

        if (this.count >= this.k) {
            this.$input.attr('disabled', 'disabled');
            this.$ask.attr('disabled', 'disabled');
        } else {
            this.$input.removeAttr('disabled');
            this.$ask.removeAttr('disabled');
        }

       if (userSuggestion < this.lower) {
            this.$history.append('<div>x is larger than ' + userSuggestion + '</div>');
        } else if (userSuggestion > this.upper) {
            this.$history.append('<div>x is smaller than ' + userSuggestion + '</div>');
        } else if (userSuggestion === this.lower && userSuggestion === this.upper) {
            this.$history.append('<div>Congratulations! x is equal to ' + userSuggestion + '</div>');

            if (this.count <= this.k) {
                this._cb();
            } else {
                $modal.modal('show');
            }
        } else if (this.upper - userSuggestion > userSuggestion - this.lower) {
            this.$history.append('<div>x is larger than ' + userSuggestion + '</div>');
            this.lower = Number(userSuggestion) + 1;
            this.lower = this.lower > this.upper ? this.upper : this.lower;
        } else if (this.upper - userSuggestion < userSuggestion - this.lower) {
            this.$history.append('<div>x is smaller than ' + userSuggestion + '</div>');
            this.upper = Number(userSuggestion) - 1;
        } else if (this.upper - userSuggestion === userSuggestion - this.lower) {
            // if userSuggestions is the middle of the interval,
            // randomly choose the half of the interval.
            if (Math.random() > 0.5) {
                this.$history.append('<div>x is larger than ' + userSuggestion + '</div>');
                this.lower = Number(userSuggestion) + 1;
                this.lower = this.lower > this.upper ? this.upper : this.lower;
            } else {
                this.$history.append('<div>x is smaller than ' + userSuggestion + '</div>');
                this.upper = Number(userSuggestion) - 1;
            }
        }
    };

    const clock = new Clock(
        `#level`,
        LEVELS_DATA[level],
        level,
        function () {
            window.q.successCb();
        }
    );

    // var clocks = LEVELS_DATA.map(function (item, i) {
    //     return new Clock(
    //         '#level' + (i + 1),
    //         item,
    //         i + 1,
    //         function () {
    //             window.q.successCb(i + 1, data);
    //         }
    //     );
    // });
};
