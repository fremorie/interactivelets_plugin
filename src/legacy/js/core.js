import css from '../static/css/common.css';

export default function ({level}, isSubmitted) {
    // helper for getting value from cookies without libraries
    const getCookie = function (name) {
        const matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    };

    // helper for setting value to cookies without library
    const setCookie = function (name, value, options) {
        options = options || {};

        let expires = options.expires;

        if (typeof expires == "number" && expires) {
            const d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        let updatedCookie = name + "=" + value;

        for (let propName in options) {
            updatedCookie += "; " + propName;
            const propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    };

    // default success callback for all ungraded quizzes
    const successCb = function successCb() {
        // prevent communication with grader.js if puzzle is viewed on the submission page
        if (isSubmitted) {
            $('#congratulations_modal').modal('show');
            // make puzzle background green
            $('body').addClass('isCorrect');

            return;
        }

        // send successful event to coursera
        courseraApi.callMethod({
            type: "SET_ANSWER",
            data: {
                answer: {isCompleted: true}
            },
            onSuccess: () => {
                $('#congratulations_modal').modal('show');
                // make puzzle background green
                $('body').addClass('isCorrect');
            },
            onError: () => {
                $('#error_modal').modal('show');
            },
        });

        const storedKey = `isPuzzlePreviouslyCompleted${level !== undefined ? `-${level}` : ''}`;

        courseraApi.callMethod({
            type: 'SET_STORED_VALUES',
            data: {
                [storedKey]: 'true',
            },
            onSuccess: () => {},
            onError: () => {
                console.error('Failed to set stored values');
            },
        });

    };

    // adding core exportable methods to the namespace
    window.q = {
        getCookie: getCookie,
        setCookie: setCookie,
        successCb: successCb
    };
};
