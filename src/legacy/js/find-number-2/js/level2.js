import app from './app';
import core from '../../core';
import initCoursera from '../../init';

import puzzleWrapper from '../../puzzleWrapper';

puzzleWrapper({puzzle: () => app(2), height: 300});
