import app from './app';
import core from '../../core';
import initCoursera from '../../init';

import puzzleWrapper from '../../puzzleWrapper';

puzzleWrapper({puzzle: () => app(4), height: 300});
