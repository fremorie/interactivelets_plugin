exports.gradeAnswer = function(graderConfig, sessionConfig, answer) {
    const level = sessionConfig.level;
    // Prevent Coursera from displaying JS error in UI
    if (answer === null) {
        const isCorrect = false;
        const feedback = "Please try again.";

        return {
            isCorrect,
            feedback,
            feedbackConfiguration: {
                ...sessionConfig,
                [`isPuzzlePreviouslyCompleted${level !== undefined ? `-${level}` : ''}`]: isCorrect,
                feedback,
                isCorrect,
                isSubmitted: true,
            }
        }
    }
    const isCorrect = graderConfig.isCompleted === answer.isCompleted;
    const feedback = isCorrect ? "Great job!" : "Please try again.";

    return {
        isCorrect: isCorrect,
        feedback: feedback,
        feedbackConfiguration: {
            ...sessionConfig,
            [`isPuzzlePreviouslyCompleted${level !== undefined ? `-${level}` : ''}`]: isCorrect,
            feedback,
            isCorrect,
            isSubmitted: true,
        }
    };
};
