const GraphCliques = function (targetName, mode, targetSelection, cb) {
    this.mode = mode || 'rect';
    this._cb = cb || function () {
    };
    this.targetSelection = targetSelection;
    const maxSize = 600;
    this.snap = Snap(targetName + ' svg').attr({viewBox: '-40 -20 ' + maxSize + ' ' + (maxSize - 100)});
    this.error1 = $(targetName + ' .graph-cliques-error1');
    this.error2 = $(targetName + ' .graph-cliques-error2');
    this.active = {};
    this.rects = [];
    this.snap.selectAll('image').forEach(function (image, i) {
        const attr = image.attr();
        let rect;
        const radius = (~~attr.height + 20) / 2;
        rect = this.snap.circle(
            attr.x - 10 + (~~attr.width + 20) / 2,
            attr.y - 10 + (~~attr.height + 20) / 2,
            radius
        ).attr('stroke-width', 3);
        this.rects.push(rect);
        rect.attr({fill: 'transparent', id: i});

        rect.mousedown(this.imageClick.bind(this, rect, i));
    }.bind(this));
    return this;
};

GraphCliques.prototype.validate = function () {
    const active = [];
    for (let key in this.active) {
        if (this.active[key]) {
            active.push(key);
        }
    }
    for (let i = 0; i < active.length; i++) {
        const firstActive = active[i];
        for (let j = 0; j < active.length; j++) {
            const secondActive = active[j];
            if (i !== j) {
                const edge = this.snap.select('path[id="' + [firstActive, secondActive].sort().join('$') + '"]');
                if (this.mode === 'circle') {
                    if (!edge) {
                        this.error1.show();
                        this.error2.hide();
                        return;
                    }
                } else {
                    if (edge) {
                        this.error1.show();
                        this.error2.hide();
                        return;
                    }
                }
            }
        }
    }

    if (active.length !== this.targetSelection.length) {
        this.error1.hide();
        this.error2.show();
        return false;
    }

    this.error1.hide();
    this.error2.hide();
    return true;
};

GraphCliques.prototype.imageClick = function (e, i) {
    if (!this.active[i]) {
        e.attr('stroke', this.mode === 'circle' ? 'black' : 'blue');
        this.active[i] = true;
    } else {
        e.attr('stroke', 'none');
        this.active[i] = false;
    }

    return this;
};

GraphCliques.prototype.reset = function () {

    this.active = {};
    this.rects.forEach(function (rect) {
        rect.attr('stroke', 'none');
    });
    return this;
};

export default GraphCliques;
