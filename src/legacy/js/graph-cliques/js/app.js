import {LEVELS_DATA} from './constants';
import GraphCliques from './GraphCliques';

// levels 1 and 2
export default function(level) {
    const graphCliques = new GraphCliques(
        `#graphCliques${level}`,
        LEVELS_DATA[level].mode,
        LEVELS_DATA[level].target,
        function () {
            window.q.successCb();
        });

    $('.check-quiz').on('click', function() {
        if (level < 2) {
            if (graphCliques.validate()) {
                graphCliques._cb();
            }
        }
    });

    $('.reset').on('click', function() {
        if (level < 2) {
            graphCliques.reset();
        }
    });

};
