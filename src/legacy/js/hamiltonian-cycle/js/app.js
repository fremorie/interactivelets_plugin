import {dataForLevels} from './constants';

export default function(level) {

    const HamiltonianGraph = function (targetName, index, pointsForLevel, cb) {
        pointsForLevel = pointsForLevel || {
            points: [],
            ways: {},
            offset: [],
            step: [],
            type: ''
        };

        this.MAX_DIMENSION = 600;
        this.MAX_RADIUS = 260;
        this.POINT_RADIUS = 15;

        this.successNumber = pointsForLevel.points.reduce(function (sum, count) {
            return sum + count;
        }, 0);
        this.levels = pointsForLevel.points;
        this.ways = pointsForLevel.ways;
        this.offsets = pointsForLevel.offset;
        this.diffRadius = pointsForLevel.step;
        this.type = pointsForLevel.type;

        this.angles = [];
        this.n = [];
        this.radius = [];
        this.calculateAngles();

        this._cb = cb || function () {
        };
        this._targetName = targetName;
        this._id = index;

        this.snap = Snap(targetName).attr({viewBox: '0 0 ' + this.MAX_DIMENSION + ' ' + this.MAX_DIMENSION});
    };

    HamiltonianGraph.prototype.calculateAngles = function () {
        if (this.levels.length) {
            (this.levels || []).forEach(function (dots, i) {
                this.angles[i] = (360 / dots) * (Math.PI / 180);
                this.n[i] = dots;
                this.radius[i] = this.MAX_RADIUS - this.diffRadius[i] * i;
            }.bind(this));
        }
    };

    HamiltonianGraph.prototype.init = function () {
        this.circles = {};
        this.vertices;
        this.path = [];

        this.draw();

        return this;
    };

    HamiltonianGraph.prototype.drawCircles = function (isDummy) {
        if (this.circleGroup) {
            this.circleGroup.remove();
        }

        if (this.levels && this.levels.length) {
            this.circleGroup = this.snap.g().attr({ id: 'circles' });

            for (let i = 0; i < this.levels.length; i++) {
                for (let j = 0; j < this.levels[i]; j++) {
                    const x = Math.cos(j * this.angles[i] + this.offsets[i]) * (this.MAX_RADIUS - this.diffRadius[i] * i) + this.MAX_DIMENSION / 2;
                    const y = Math.sin(j * this.angles[i] + this.offsets[i]) * (this.MAX_RADIUS - this.diffRadius[i] * i) + this.MAX_DIMENSION / 2;
                    const id = i + '' + j;

                    const circle = this.snap
                        .circle(x, y, this.POINT_RADIUS)
                        .attr({'data-id': id, class: 'circle'})
                        .mousedown(isDummy ? function () {
                        } : this._handleCircleClick.bind(this));

                    this.circles[id] = circle;
                    this.circleGroup.add(circle);
                }
            }
        }

        return this;
    };

    HamiltonianGraph.prototype._handleCircleClick = function (event) {
        event.preventDefault();

        const id = event.target.getAttribute('data-id');

        const isFirstPoint = !this.path.length;
        const isPossibleWayFromPoint = !isFirstPoint && Object.keys(this.ways).length &&
            this.ways[this.path[this.path.length - 1]].indexOf(id) !== -1 &&
            this.path.indexOf(id) === -1;
        const isClosingPoint = this.type === 'cycle' &&
            this.path.length === this.successNumber &&
            this.path.indexOf(id) === 0 &&
            this.ways[this.path[this.path.length - 1]].indexOf(id) !== -1;
        const isUndo = this.path.length && this.path[this.path.length - 1] === id;

        if (isFirstPoint || isPossibleWayFromPoint || isClosingPoint || isUndo) {
            if (this.path.indexOf(id) !== -1) {
                if (this.path.indexOf(id) === this.path.length - 1) {
                    this._spliceVertex();
                } else if (this.type === 'cycle') {
                    if (this.path.length === this.successNumber) {
                        if (this.path.indexOf(id) === 0) {
                            this._addVertex(id);

                            this._cb(this._id);
                        }
                    } else {
                        this._addVertex(id);
                    }
                }
            } else {
                this._addVertex(id);

                if (this.type === 'path') {
                    if (this.path.length === this.successNumber) {
                        this._cb(this._id);
                    }
                }
            }
        }
    };

    HamiltonianGraph.prototype._spliceVertex = function () {
        this.path.splice(this.path.length - 1);

        this.draw();
    };

    HamiltonianGraph.prototype._addVertex = function (id) {
        this.path.push(id);

        this.draw();
    }

    HamiltonianGraph.prototype.draw = function () {
        this.drawCircles(true);
        this.drawBackVerticles();
        this.drawVertices();
        this.drawCircles();
        this.drawActive();
        this.drawWeights();
    }

    HamiltonianGraph.prototype.drawWeights = function () {
        if (this.textGroup) {
            this.textGroup.remove();
        }

        if (this.path.length) {
            this.textGroup = this.snap.g().attr({ id: 'weights' });

            this.path.forEach(function (id, i) {
                const x = Number(this.circles[id].attr('cx'));
                const y = Number(this.circles[id].attr('cy'));

                const text = this.snap
                    .text((x - 4), (y + 5), i + 1)
                    .attr({'font-size': 15, 'fill': '#fff'});

                this.textGroup.add(text);
            }.bind(this));
        }
    }

    HamiltonianGraph.prototype.drawBackVerticles = function () {
        if (this.backVerticlesGroup) {
            this.backVerticlesGroup.remove();
        }

        if (this.ways && Object.keys(this.ways).length) {
            this.backVerticlesGroup = this.snap.g().attr({ id: 'back-verticles' });

            const a = Object.keys(this.ways);

            a.forEach(function (i) {
                const x1 = this.circles[i].attr('cx');
                const y1 = this.circles[i].attr('cy');

                if (this.ways[i] && this.ways[i].length) {
                    this.ways[i].forEach(function (j) {
                        const x2 = this.circles[j].attr('cx');
                        const y2 = this.circles[j].attr('cy');

                        this.backVerticlesGroup.add(this.snap
                            .path('M' + [x1, y1, x2, y2].toString())
                            .attr({
                                stroke: '#5bc0de',
                                'stroke-width': '5',
                                fill: 'none'
                            })
                        );
                    }.bind(this));
                }
            }.bind(this));
        }

        return this;
    }

    HamiltonianGraph.prototype.drawActive = function () {
        const lastCircle = this.circles[this.path[this.path.length - 1]];

        Object.keys(this.circles).forEach(function (key) {
            this.circles[key].removeClass('active');
        }.bind(this));

        lastCircle && lastCircle.addClass('active');
    };

    HamiltonianGraph.prototype.drawVertices = function () {
        const path = [];
        const firstIndex = String((Object.keys(this.circles) || [])[0]);

        if (this.path.length > 1) {
            this.path.forEach(function (key) {
                const x = this.circles[key].attr('cx');
                const y = this.circles[key].attr('cy');

                path.push(x, y);
            }.bind(this));

            if (this.vertices) {
                this.vertices.remove();
            }

             this.vertices = this.snap
                .path('M' + path.toString())
                .attr({
                    stroke: '#b14899',
                    'stroke-width': '5',
                    fill: 'none'
                });
        }
    };

    const graph = new HamiltonianGraph(
        '#graph' + (level + 1),
        level,
        dataForLevels[level],
        function () {
            window.q.successCb();
        }).init();

    $('.reset').on('click', function() {
        graph.init();
    });

    $('#congratulations_modal').on('show.bs.modal', function (e) {
        if (e.relatedTarget) {
            if (level === 1) {
                window.q.successCb();
            }
        }
    });
};
