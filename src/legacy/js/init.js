export default function initCoursera({height = 600, level, isPreviouslyCompleted}, isSubmitted) {
    courseraApi.callMethod({
        type: "SET_HEIGHT",
        data: {
            height: `${height}px`
        }
    });

    if (isPreviouslyCompleted) {
        // make puzzle background green
        $('body').addClass('isCorrect');
        return;
    }

    if (isPreviouslyCompleted === undefined) {
        /**
         * isPreviouslyCompleted may be obtained from session configuration
         * when a learner views plugin on the results page.
         * For cases when plugin is being displayed on the quiz page,
         * we have to make an additional call (GET_STORED_VALUES).
         * */
        const storedKey = `isPuzzlePreviouslyCompleted${level !== undefined ? `-${level}` : ''}`;

        courseraApi.callMethod({
            type: "GET_STORED_VALUES",
            data: [storedKey],
            onSuccess: function(values) {
                if (values[storedKey] === 'true') {
                    // make puzzle background green
                    $('body').addClass('isCorrect');

                    // prevent communication with grader.js if puzzle is viewed on the submission page
                    if (!isSubmitted) {
                        courseraApi.callMethod({
                            type: "SET_ANSWER",
                            data: {
                                answer: {isCompleted: true}
                            },
                            onSuccess: () => {},
                            onError: () => {
                                console.error('Failed to set answer');
                            },
                        });
                    }
                }
            },
            onError: function() {
                console.error('Failed to get stored values');
            }
        });
    }
}
