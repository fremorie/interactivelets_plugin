import {LEVELS} from './constants';

export default function(level) {
    function initLevel(id) {
        const items = LEVELS[id].i.reduce(function (acc, value) {
            return acc + '<div class="sortable-item">' + value + '</div>';
        }, '');

        $('#level' + id + ' .content')
            .html(items)
            .sortable({
                stop: function(event, ui) {
                    const $activePane = $('.tab-pane.active');
                    const level = $activePane.attr('id').replace('level', '');
                    const currentValues = $activePane
                        .find('.sortable-item')
                        .map(function (id, item) {
                            return Number(item.innerText);
                        });
                    const targetValues = LEVELS[level].s;

                    let success = true;

                    for (let i = 0; i < targetValues.length; i++) {
                        if (targetValues[i] !== currentValues[i]) {
                            success = false;
                        }
                    }

                    if (success) {
                        window.q.successCb();
                    }
                }
            })
            .disableSelection();
    }

    initLevel(level);

    $('.reset').click(function() {
        initLevel(level);
    })
};
