export const LEVELS = {
    1: {i: [2, 3, 9], s: [9, 3, 2]},
    2: {i: [6, 61, 68], s: [68, 6, 61]},
    3: {i: [4, 42, 46, 427, 465], s: [465, 46, 4, 427, 42]},
    4: {i: [5, 52, 57, 517, 532, 569, 581], s: [581, 57, 569, 5, 532, 52, 517]}
};
