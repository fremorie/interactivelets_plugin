import img from '../../../static/images/queen.png';

export default function(level) {

    const Queens = function (targetName, count, cb) {
        const maxSize = 600;

        this.snap = Snap(targetName).attr({viewBox: '0 0 ' + maxSize + ' ' + maxSize});
        this.n = count;
        this.size = maxSize / this.n;
        this._cb = cb || function () {
        };

        this.flush();
    };

    Queens.prototype.init = function () {
        for (let i = 0; i < this.n; i++) {
            for (let j = 0; j < this.n; j++) {
                this.cells.push(this.snap
                    .rect(i * this.size, j * this.size, this.size, this.size)
                    .attr('fill', !(i % 2) && j % 2 || i % 2 && !(j % 2) ? 'sienna' : 'peachpuff')
                );

                this.queenImages.push(this.snap
                    .image('queen.png', i * this.size, j * this.size, this.size, this.size)
                    .attr({ display: 'none' })
                );

                this.invisibleCells.push(this.snap
                    .rect(i * this.size, j * this.size, this.size, this.size)
                    .attr({
                        fill: this.snap
                            .path('M 10,-5 L -10,15 M 15,0 L 0,15 M 0,-5 L -20,15')
                            .attr({
                                fill: 'none',
                                stroke: 'black',
                                strokeWidth: 4
                            })
                            .pattern(0, 0, 10, 10),
                        opacity: 0,
                        i: i,
                        j: j
                    })
                    .mousedown(function (event) {
                        event.preventDefault();

                        const i = Number(event.target.getAttribute('i'));
                        const j = Number(event.target.getAttribute('j'));

                        if (this.attackedCells[i * this.n + j] === 0 && this.queens[i * this.n + j] === 0) {
                            this.queens[i * this.n + j] = 1;
                        } else {
                            if (this.queens[i * this.n + j] == 1) {
                                this.queens[i * this.n + j] = 0;
                            }
                        }

                        if (this.queens.reduce(function (result, item) { return result + item; }, 0) === this.n) {
                            this._cb(this.n);
                        }

                        this.redraw();
                    }.bind(this))
                );
            }
        }

        return this;
    };

    Queens.prototype.redraw = function () {
        this.attackedCells = Array.apply(null, Array(this.n * this.n)).map(Number.prototype.valueOf, 0);

        this.queenImages.forEach(function (queen, k) {
            if (this.queens[k]) {
                queen.attr({ display: '' });

                const i = (k % this.n);
                const j = Math.floor(k / this.n);

                for (let deltai = -1; deltai < 2; ++deltai) {
                    for (let deltaj = -1; deltaj < 2; ++deltaj) {
                        if (deltai == 0 && deltaj == 0) {
                            continue;
                        }

                        let tmpi = i;
                        let tmpj = j;

                        while (0 <= tmpi && tmpi < this.n && 0 <= tmpj && tmpj < this.n) {
                            this.attackedCells[tmpj * this.n + tmpi] = 1;
                            tmpi += deltai;
                            tmpj += deltaj;
                        }
                    }
                }
            } else {
                queen.attr({ display: 'none' });
            }
        }.bind(this));

        for (var k = 0; k < this.n * this.n; k++) {
            this.invisibleCells[k].attr({ opacity: this.attackedCells[k] && !this.queens[k] ? 0.8 : 0 });
        }

        return this;
    };

    Queens.prototype.flush = function () {
        this.queens = Array.apply(null, Array(this.n * this.n)).map(Number.prototype.valueOf, 0);
        this.attackedCells = Array.apply(null, Array(this.n * this.n)).map(Number.prototype.valueOf, 0);

        this.invisibleCells = [];
        this.queenImages = [];
        this.cells = [];

        return this;
    };

    const data = [2, 3, 4, 5, 8];

    const queen = new Queens(
        `#queen`,
        data[level],
        function () {
            window.q.successCb();
        }).init();

    $('.reset').on('click', function() {
        queen.flush().init();
    });

    $('#impossible').on('click', function (e) {
        if (e.currentTarget) {
            if (level < 2) {
                window.q.successCb();
            } else {
                $('#possible_modal').modal('show');
            }
        }
    });
};
