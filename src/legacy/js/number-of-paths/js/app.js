export default function(level) {
    const NumberOfPaths = function (targetName, prev, fontSize, targetValue, cb) {
        this.snap = Snap($(targetName + ' svg')[0]);
        this.targetValue = targetValue;
        this.input = $(targetName + ' input');
        this.input.keyup(this.onInput.bind(this));
        this.prev = prev;
        this.textGroup = this.snap.g();
        this.circleGroup = this.snap.g();
        this.circles = [];
        this.texsts = [];
        this.active = null;
        this.backCircles = [];
        this.snap.selectAll('ellipse').forEach(function (el, i) {
            const attr = el.attr();
            this.backCircles.push(el);
            const text = this.snap.text(~~attr.cx - fontSize / 2, ~~attr.cy + fontSize / 4, this.isStart(i) ? '1' : '')
                .attr('font-size', fontSize)
                .prepend(this.group);
            text.attr({
                'data-cx': attr.cx,
                'data-cy': attr.cy,
                x: (~~attr.cx - fontSize / 4),
                y: (~~attr.cy + fontSize / 4)
            });
            this.texsts.push(text);
            const circle = this.snap.circle(
                attr.cx,
                attr.cy,
                attr.rx
            )
                .attr({fill: 'transparent', stroke: 'none'})
                .click(this.handleClick.bind(this, i))
                .prepend(this.circleGroup);

            this.circles.push(circle);

        }.bind(this));

        this._cb = cb || function () {
        };
    };

    NumberOfPaths.prototype.setText = function (i, value) {
        const text = this.texsts[i];
        const attr = text.attr();
        const cx = attr['data-cx'];
        const cy = attr['data-cy'];
        text.attr({ text: (value + '').slice(0,2) });
        const bBox = text.getBBox();
        text.attr({
            x: (~~cx - bBox.width / 2 ),
            y: (~~cy + bBox.height / 4),
        });

    };

    NumberOfPaths.prototype.isStart = function (i) {
        return !this.prev(i).length;
    };

    NumberOfPaths.prototype.onInput = function (e) {
        const value = ~~e.target.value || '';
        if (this.active != null) {
            this.setText(this.active, value > 0 ? value : '');
        }
    };

    NumberOfPaths.prototype.activate = function (i) {
        this.backCircles[i].attr('fill', 'green');

        this.input.val($(this.texsts[i].node).text());
        this.input.attr({ disabled: false });
        this.input.focus().select();
        this.active = i;
    };

    NumberOfPaths.prototype.invalidate = function (i) {
        this.backCircles[i].attr('fill', 'red');
    };

    NumberOfPaths.prototype.isValid = function (i) {
        const text = this.texsts[i];
        const value = ~~$(text.node).text();
        let actialValue = 0;
        const prevs = this.prev(i);
        for (let j = 0; j < prevs.length; j++) {
            const prevId = prevs[j];
            const prevValue = ~~$(this.texsts[prevId].node).text();
            actialValue += prevValue
        }
        return actialValue && actialValue == value;
    };

    NumberOfPaths.prototype.check = function () {
        this.diactivate();
        if (this.targetValue) {
            const targetText = this.texsts[this.targetValue.id];
            const value = ~~$(targetText.node).text();
            if (value == this.targetValue.value) {
                return true;
            }
        }

        let result = true;
        for (let j = 0; j < this.texsts.length; j++) {
            if (!this.isStart(j)) {
                if (!this.isValid(j)) {
                    this.invalidate(j);
                    result = false;
                }
            }
        }

        return result;
    };

    NumberOfPaths.prototype.diactivate = function () {
        this.input.attr({ disabled: true });
        this.input.val('');
        if (this.active != null) {
            this.backCircles[this.active].attr('fill', 'white');
            this.active = null;
        }
    };

    NumberOfPaths.prototype.handleClick = function (i) {
        if (!this.prev(i).length) {
            return;
        }
        this.diactivate();
        this.activate(i);
    };

    NumberOfPaths.prototype.init = function () {
        return this;
    };

    NumberOfPaths.prototype.flush = function () {
        this.texsts.forEach(function (text, i) {
            if (!this.isStart(i)) {
                this.setText(i, '');
                this.diactivate();
                this.backCircles.forEach(function (back) {
                    back.attr('fill', 'white');
                })
            }
        }.bind(this));
        return this;
    };

    const data = [0, 1, 2, 3];
    const fontSizes = [60, 40, 60, 55];
    const answers = [undefined, {id: 4, value: 35}, {id: 3, value: 40}, {id: 5, value: 16}];
    var prevs = [function (i) {
        const prev = {
            0: [],
            1: [0],
            2: [0],
            3: [0, 2],
            4: [1, 3],
            5: [2],
            6: [4, 5],
            7: [4],
            8: [5, 6],
            9: [6, 7, 8],
        };
        return prev[i];
    }, function (i) {
        const prevs = [];
        const prevA = Math.max(0, i - 1);
        if (prevA >= 0 && (i % 5 > 0)) {
            prevs.push(prevA);
        }
        const prevB = i + 5;
        if (prevB < 20) {
            prevs.push(prevB)
        }
        return prevs;
    }, function (i) {
        const prev = {
            0: [],
            1: [0, 0, 0, 0],
            2: [1, 1],
            3: [2, 2, 2, 2, 2]
        };
        return prev[i];
    }, function (i) {
        const prev = [];
        for (let j = 0; j < i; j++) {
            prev.push(j);
        }
        return prev;
    }];

    const game = new NumberOfPaths(
        `#numberOfPaths${level}`,
        prevs[level],
        fontSizes[level],
        answers[level],
        function () {
            window.q.successCb();
        }).init();

    $('.reset').on('click', function () {
        game.flush().init();
    });

    $('.submit-map').on('click', function () {
        if (game.check()) {
            window.q.successCb();
        }
    });
};
