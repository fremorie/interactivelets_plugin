import puzzleWrapper from "../../puzzleWrapper";

const app = function () {
    const fields = $('.numbers-on-chessboard-content .field');
    const fieldsMatrix = [];
    for (var i = 0; i < 8; i++) {
        fieldsMatrix[i] = [];
        for (var j = 0; j < 8; j++) {
            fieldsMatrix[i][j] = fields[8 * i + j];
        }
    }

    $('.reset').on('click', function () {
        fields.val('').removeClass('invalid');
    });

    function isValid(input) {
        const $input = $(input);
        const value = ~~input.value;
        return value > 0 && value < 64 && !$input.hasClass('invalid');
    }

    fields.keyup(function (e) {
        const used = {};
        fields.removeClass('invalid');
        fields.toArray().forEach(function (input, i) {
            const $input = $(input);
            const value = ~~input.value;
            if (input.value === '') {
                return;
            }
            if (!isValid(input) || used[value]) {
                if (used[value]) {
                    used[value].addClass('invalid');
                }
                $input.addClass('invalid');
                return;
            }
            used[value] = $input;
            getNeighbors(Math.floor(i / 8), i % 8).forEach(function (neighbor) {
                if (!isValid(input)) {
                    return;
                }
                const neighborValue = ~~neighbor.value;
                if (neighborValue - value > 4) {
                    $input.addClass('invalid');
                    $(neighbor).addClass('invalid');
                }
            });
        });

    });

    function getNeighbors(i, j) {
        const neighbors = [];

        if (fieldsMatrix[i - 1]) {
            neighbors.push(fieldsMatrix[i - 1][j]);
        }
        if (fieldsMatrix[i][j - 1]) {
            neighbors.push(fieldsMatrix[i][j - 1]);
        }
        if (fieldsMatrix[i][j + 1]) {
            neighbors.push(fieldsMatrix[i][j + 1]);
        }
        if (fieldsMatrix[i + 1]) {
            neighbors.push(fieldsMatrix[i + 1][j]);
        }
        return neighbors;
    }

    $('#congratulations_modal').on('show.bs.modal', function (e) {
        if (e.relatedTarget) {
            window.q.successCb(1, [1]);
        }
    });
};

puzzleWrapper({puzzle: app, height: 620});
