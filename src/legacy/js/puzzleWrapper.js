import core from './core';
import initCoursera from './init';

export default function({puzzle, height}) {
    courseraApi.callMethod({
        type: 'GET_SESSION_CONFIGURATION',
        onSuccess: config => {
            const {level, isSubmitted} = config;
            const isPreviouslyCompleted = config[`isPuzzlePreviouslyCompleted${level === undefined ? '' : `-${level}`}`];

            core({level}, isSubmitted);
            initCoursera({height, level, isPreviouslyCompleted}, isSubmitted);
            puzzle(level);
        },
        onError: error => {
            console.error(`Failed to get session configuration, traceback: ${error}`)
        },
    });
};
