export default function(level) {
    const Graph = function (targetName, count, cb) {
        this.maxDimension = 600;
        this.radius = 260;
        this.circleRadius = 40;
        this.successNumber = 5;

        this.snap = Snap(targetName).attr({viewBox: '0 0 ' + this.maxDimension + ' ' + this.maxDimension});
        this.angle = (360 / count) * (Math.PI / 180);
        this.n = count;

        this._cb = cb || function () {
        };
        this._targetName = targetName;
    };

    Graph.prototype.init = function () {
        this.circles = [];
        this.vertices = [];
        this.counters = Array.apply(null, Array(this.n)).map(Number.prototype.valueOf, 0);
        this.current = '';

        this.drawCircles();
        this.drawCounters();
        this.drawVertices();

        return this;
    };

    Graph.prototype.drawCircles = function () {
        if (this.circleGroup) {
            this.circleGroup.remove();
        }

        this.circleGroup = this.snap.g().attr({ id: 'circles' });

        for (let i = 0; i < this.n; i++) {
            const x = Math.cos(i * this.angle) * this.radius + this.maxDimension / 2;
            const y = Math.sin(i * this.angle) * this.radius + this.maxDimension / 2;
            const circle = this.snap
                .circle(x, y, this.circleRadius)
                .attr({'data-id': i, class: 'circle'})
                .mousedown(this._handleCircleClick.bind(this));

            this.circles.push(circle);
            this.circleGroup.add(circle);
        }

        return this;
    };

    Graph.prototype.drawCounters = function () {
        if (this.countersGroup) {
            this.countersGroup.remove();
        }

        this.countersGroup = this.snap.g().attr({ id: 'counters' });

        for (let i = 0; i < this.n; i++) {
            var text = this.counters[i].toString();
            const x = Math.cos(i * this.angle) * this.radius + this.maxDimension / 2 - 10;
            const y = Math.sin(i * this.angle) * this.radius + this.maxDimension / 2 + 12;
            var text = this.snap
                .text(x, y, this.counters[i].toString())
                .attr({ 'font-size': 35, 'fill': '#fff' });

            this.countersGroup.add(text);
        }

        return this;
    };

    Graph.prototype.drawVertices = function () {
        if (this.verticesGroup) {
            this.verticesGroup.remove();
        }

        this.verticesGroup = this.snap.g().prependTo(this.snap).attr({ id: 'vertices' });

        this.vertices.forEach(function(vertice) {
            const ids = vertice.split(',');
            const line = this.snap
                .line(
                    this.circles[ids[0]].getBBox().cx,
                    this.circles[ids[0]].getBBox().cy,
                    this.circles[ids[1]].getBBox().cx,
                    this.circles[ids[1]].getBBox().cy)
                .attr({
                    stroke: '#5bc0de',
                    strokeWidth: '7',
                    class: 'cirle'
                });

            this.verticesGroup.add(line);
        }, this);

        return this;
    };

    Graph.prototype._handleCircleClick = function (event) {
        event.preventDefault();

        const id = event.target.getAttribute('data-id');

        $(this._targetName + ' .circle').removeClass('active');

        if (!this.vertices.length && this.current === id) {
            this.current = '';

            return this;
        }

        if (this.current && this.current !== id) {
            const vertice = this.current + ',' + id;
            const reversedVertice = id + ',' + this.current;
            const indexOfVertice = this.vertices.indexOf(vertice);
            const indexofReversedVertice = this.vertices.indexOf(reversedVertice);

            if (indexOfVertice === -1 && indexofReversedVertice === -1) {
                this.vertices.push(vertice);
                this.changeCounters('+', this.current, id);
            } else {
                if (indexOfVertice !== -1) {
                    this.vertices = (
                        this.vertices.slice(0, indexOfVertice) || []).concat(this.vertices.slice(indexOfVertice + 1)
                    );
                } else {
                    this.vertices = (
                        this.vertices.slice(0, indexofReversedVertice) || []).concat(this.vertices.slice(indexofReversedVertice + 1)
                    );
                }
                this.changeCounters('-', this.current, id);
            }

            this.drawVertices();
            this.drawCounters();

            this.current = '';
        } else {
            this.current = id;
            $(event.target).addClass('active');
        }
    };

    Graph.prototype.changeCounters = function(type, current, id) {
        if (type === '+') {
            this.counters[current]++;
            this.counters[id]++;
        } else {
            this.counters[current]--;
            this.counters[id]--;
        }

        this.checkOnSuccess();
    };

    Graph.prototype.checkOnSuccess = function() {
        const isOk = this.counters.reduce(function (result, counter) {
            return result + (Number(counter) === this.successNumber);
        }.bind(this), 0) === this.counters.length;

        if (isOk) {
            this._cb(this.n);
        }
    };

    const circlesCount = [10, 9];

    const graph = new Graph(
        '#graph',
        circlesCount[level],
        function () {
            window.q.successCb();
        }).init();

    $('.reset').on('click', function() {
        graph.init();
    });

    $('#impossible').click(function (e) {
        e.preventDefault();
        if (e.target) {
            if (level === 1) {
                window.q.successCb();
            } else {
                $('#possible_modal').modal('show');
            }
        }
    });
};
