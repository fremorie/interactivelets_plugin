export default function app(level) {
    const $modal = $('#congratulations_modal');

    const data = level === 0 ? [1, 2, 3, 4, 5, 7] : [1, 2, 3, 4, 5, 6];

    const numElements = document.querySelectorAll('[id^=num-]');
    numElements.forEach((element, i) => element.innerHTML = data[i]);
    const resultElement = document.querySelector('#result');
    resultElement.innerHTML = level === 0 ? '22' : '21';

    $('.tab-pane').on('click', 'input[type="checkbox"]', function () {
        const $pane = $('.tab-pane.active');
        const $result = $pane.find('#result');
        const $checkboxes = $pane.find('input[type="checkbox"]');

        const result = $checkboxes.toArray().reduce(function (result, checkbox, i) {
            const multiplicator = $(checkbox).prop('checked') ? 1 : -1;

            return result + (multiplicator * data[i]);
        }, 0);

        if (!result) {
            window.q.successCb();
        }

        $result.text(result);
    });

    $('.tab-pane').on('click', '.reset', function () {
        const $pane = $('.tab-pane.active');
        const $result = $pane.find('#result');
        const $checkboxes = $pane.find('input[type="checkbox"]');
        const result = level === 0 ? 22 : 21;

        $checkboxes.each(function(){ this.checked = true; });
        $result.text(result);
    });

    $('.impossible').on('click', function (e) {
        if (level === 1) {
            window.q.successCb();
        } else {
            $('#possible_modal').modal('show');
        }
    });
}
