export default function(level) {
    var targetSum = [0, 1, 2, 100];

    function SummingUpDigits(selector, targetSum, cb) {
        this._cb = cb;
        this.targetSum = targetSum;
        this.signs = $(selector).find('input[type="checkbox"]');
        this.signs.click(this.recalculate.bind(this));
        this.display = $(selector).find('#result');
    }

    SummingUpDigits.prototype.init = function () {
        this.signs.toArray().forEach(function (checkbox, i) {
            checkbox.checked = !(i % 2)
        });
        this.recalculate();
        return this;
    };

    SummingUpDigits.prototype.recalculate = function () {
        var sum = this.signs.toArray().reduce(function (sum, checkbox, i) {
            return sum + ~~(checkbox.checked ? i + 1 : -(i + 1))
        }, 0);
        if (sum == this.targetSum) {
            this._cb();
        }
        this.display.text(sum);
    };

    SummingUpDigits.prototype.flush = function () {
        return this;
    };

    const summing = new SummingUpDigits(
        `#level`,
        targetSum[level],
        function () {
            window.q.successCb();
        }).init();

    $('.reset').on('click', function () {
        summing.flush().init();
    });

    $('#impossible').click(function (e) {
        e.preventDefault();
        if (e.target) {
            if (level !== 1) {
                window.q.successCb();
            } else {
                $('#possible_modal').modal('show');
            }
        }
    });
};
