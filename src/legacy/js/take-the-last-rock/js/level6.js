import initCoursera from '../../init';
import core from '../../core';
import app from './app';

import puzzleWrapper from "../../puzzleWrapper";

puzzleWrapper({puzzle: () => app(5), height: 350});
