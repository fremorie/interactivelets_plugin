import initCoursera from '../../init';
import core from '../../core';
import app from './app';

import puzzleWrapper from "../../puzzleWrapper";

puzzleWrapper({puzzle: () => app(6), height: 350});
