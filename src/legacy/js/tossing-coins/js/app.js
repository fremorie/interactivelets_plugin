export default function(level) {
    const TossingCoinsLevel = function (solution, id, successCb) {
        const levelNode = document.getElementById(id);

        this.buttonNodes = levelNode.querySelectorAll('.tossing-coins__button');
        this.probabilityTextNode = levelNode.querySelector('.tossing-coins__probability-text');

        this._successCb = successCb;
        this._initialState = [true, false, false, false, false, false, false, false];
        this.state = this._initialState.slice();
        this.solution = solution;

        this.init();
        this.render();
    };

    TossingCoinsLevel.prototype.changeState = function (buttonIndex) {
        this.state[buttonIndex] = !this.state[buttonIndex];
    };

    TossingCoinsLevel.prototype.render = function () {
        const activeClassName = 'btn-primary';

        this.state.forEach(function (isButtonActive, buttonIndex) {
            const buttonNode = this.buttonNodes.item(buttonIndex);
            if (isButtonActive) buttonNode.classList.add(activeClassName);
            else buttonNode.classList.remove(activeClassName);
        }, this);

        this.updateProbabilityText();
    };

    TossingCoinsLevel.prototype.updateProbabilityText = function () {
        const probability = this.state.reduce(
            function (p, isActive) {
                if (isActive) return p + 1;
                return p;
            },
            0
        );
        this.probabilityTextNode.innerText = 'Probability = ' + (probability / 8);
    };

    TossingCoinsLevel.prototype.checkIfSolved = function () {
        let isSolved = true;
        this.state.forEach(function (isActive, buttonIndex) {
            const shouldBeActive = Boolean(~this.solution.indexOf(buttonIndex));
            if ((isActive && !shouldBeActive) || (!isActive && shouldBeActive)) isSolved = false;
        }, this);

        if (isSolved) this._successCb();
    };

    TossingCoinsLevel.prototype.init = function () {
        const self = this;
        const createClickHandler = function (buttonIndex) {
            return function () {
                self.changeState(buttonIndex);
                self.render();
                self.checkIfSolved();
            };
        };

        for (let i = 0; i < this.buttonNodes.length; i++) {
            this.buttonNodes.item(i).addEventListener('click', createClickHandler(i))
        }
    };

    TossingCoinsLevel.prototype.reset = function () {
        this.state = this._initialState.slice();
        this.render();
    };

    // HHH, HHT, HTH, HTT, THH, THT, TTH, TTT
    const levelsData = [
        [0, 1, 2, 4],
        [1, 2, 3, 4, 5, 6, 7],
        [],
        [0, 3, 5, 6],
        [7]
    ];

    const app = new TossingCoinsLevel(
        levelsData[level],
        'level',
        function () {
            window.q.successCb();
        }
    );

    // const levels = levelsData.map(function (solution, i) {
    //     const id = i + 1;
    //     const level = new TossingCoinsLevel(
    //         solution,
    //         'level' + id,
    //         function () {
    //             window.q.successCb(id, [1, 2, 3, 4, 5]);
    //         }
    //     );
    //     return level;
    // });


    $('.reset').on('click', function() {
        app.reset();
    });
};
