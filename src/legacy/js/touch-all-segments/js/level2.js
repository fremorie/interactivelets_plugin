import app from './app';

import initCoursera from '../../init';
import core from '../../core';

import puzzleWrapper from "../../puzzleWrapper";

puzzleWrapper({puzzle: () => app(1), height: 550});
