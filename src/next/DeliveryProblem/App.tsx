import React from 'react';
import {toJS} from 'mobx';
import gsap from 'gsap';
import {Button} from 'react-bootstrap';
import {observer} from "mobx-react"

// types
import {State, Direction, Border, Coord} from './types';

// styles & images
import './styles/index.scss';
import './images/snowplow.svg';

// constants
import {LEVELS, GRID_SIZE, PLOW_TRUCK_SIZE} from './constants';
import {Store} from './store';

// utils
import {getAngleBetweenVectors} from './utils';

// components
import {CongratulationsModal} from '../common/components/CongratulationsModal';
import {FailureModal} from '../common/components/FailureModal';

const store = new Store();

const moveTruckToPoint = (level: number, elem: HTMLImageElement, targetPoint: Coord, prevPoint?: Coord, duration?: number) => {
    let rotation = 0;

    if (prevPoint) {
        const processedPrevPoint = {
            x: prevPoint.x,
            // @ts-ignore
            y: CANVAS_HEIGHT[level] - prevPoint.y,
        };

        const processedTargetPoint = {
            x: targetPoint.x,
            // @ts-ignore
            y: CANVAS_HEIGHT[level] - targetPoint.y,
        }

        rotation = getAngleBetweenVectors(processedPrevPoint, processedTargetPoint);
    }

    gsap.to(elem, {
        duration: duration ? duration : 0.5,
        rotation: `${rotation}_short`,
    })
    gsap.to(elem, {
        delay: duration ? duration : 0.5,
        x: targetPoint.x * GRID_SIZE - PLOW_TRUCK_SIZE / 3,
        // @ts-ignore
        y: CANVAS_HEIGHT[level] - (targetPoint.y * GRID_SIZE + PLOW_TRUCK_SIZE / 3),
        duration: 1,
    });
};

const CANVAS_HEIGHT = {
    0: 300,
    1: 600,
    2: 500,
    3: 600,
    4: 800,
};

const CANVAS_WIDTH = {
    0: 300,
    1: 600,
    2: 600,
    3: 600,
    4: 600,
};

type PointProps = {
    x: number,
    y: number,
    handleClick: () => void,
    isDisabled: boolean,
    coordinatesVisible: boolean,
    level: number,
}

const Point = observer(({x, y, handleClick, isDisabled, coordinatesVisible, level}: PointProps) => {
    return (
        <>
            <div
                className={`point ${isDisabled ? 'pointDisabled' : ''}`}
                onClick={() => handleClick()}
                // @ts-ignore
                style={{top: CANVAS_HEIGHT[level] - y * GRID_SIZE, left: x * GRID_SIZE}}
            />
            {coordinatesVisible && (
                <div
                    className="pointLegend"
                    // @ts-ignore
                    style={{top: CANVAS_HEIGHT[level] - y * GRID_SIZE + 15, left: x * GRID_SIZE}}
                >
                    ({x}, {y})
                </div>
            )}
        </>
    );
});

const Points = observer(({coords, handlePointClick, children, getIsPointDisabled, coordinatesVisible, level}: {
    coords: number[][],
    handlePointClick: (p: { x: number; y: number }) => void,
    children: React.ReactNode,
    getIsPointDisabled: (point: Coord) => boolean,
    coordinatesVisible: boolean,
    level: number,
}) => {

    return (
        <div className="pointsContainer">
            {coords.map(point => {
                const [x, y] = point;

                return (
                    <Point
                        key={`${x}-${y}`}
                        x={x}
                        y={y}
                        handleClick={() => handlePointClick({x, y})}
                        isDisabled={getIsPointDisabled({x, y})}
                        level={level}
                        coordinatesVisible={coordinatesVisible}
                    />
                );
            })}
            {children}
        </div>
    );
});

const Canvas = ({canvasRef, from, to, level}: {canvasRef: any, from: Coord, to: Coord, level: number}) => {
    const canvas = canvasRef.current;

    const draw = () => {
        const ctx = canvas.getContext('2d');
        ctx.lineWidth = 3;
        ctx.beginPath();
        // @ts-ignore
        ctx.moveTo(from.x * GRID_SIZE, CANVAS_HEIGHT[level] - from.y * GRID_SIZE);
        // @ts-ignore
        ctx.lineTo(to.x * GRID_SIZE, CANVAS_HEIGHT[level] - to.y * GRID_SIZE);
        ctx.stroke();
    }

    if (from && to) {
        draw();
    }

    // @ts-ignore
    return <canvas width={`${CANVAS_WIDTH[level]}px`} height={`${CANVAS_HEIGHT[level]}px`} id="canvas" ref={canvasRef} />
}

type Props = {
    store: Store,
}

const GameView = observer(({store}: Props) => {
    const [level, setLevel] = React.useState(null);

    const snowplow = React.useRef(null);

    const [showSuccessModal, setShowSuccessModal] = React.useState(false);
    const [showFailureModal, setShowFailureModal] = React.useState(false);
    const [isCoordVisible, setCoordsVisible] = React.useState(false);

    const closeSuccessModal = () => setShowSuccessModal(false);
    const closeFailureModal = () => setShowFailureModal(false);

    const canvasRef = React.useRef(null)

    React.useEffect(() => {
       if (store.isFinished) {
           if (store.isSolved) {
               setShowSuccessModal(true);
           } else {
               setShowFailureModal(true);
           }
       }
    }, [store.path]);


    React.useEffect(() => {
        const fetchLevel = async () => {
            await store.init();
            setLevel(store.level);
        }

        fetchLevel();
    }, [store.level]);

    React.useEffect(() => {
        if (level !== null) {
            moveTruckToPoint(level, snowplow.current, toJS(store.position), null, 0);
        }
    }, [level, store.isSolved, store.isFinished]);

    if (level === null) {
        return <></>
    }

    return (
        <div className={`container container-${store.level}`}>
            <div className="center">
                <Canvas
                    level={store.level}
                    from={store.position}
                    to={store.traveledPoints.length > 0 ? store.traveledPoints[store.traveledPoints.length - 1][1] : undefined}
                    canvasRef={canvasRef}
                />
                <Points
                    coords={LEVELS[store.level].points}
                    handlePointClick={({x, y}) => {
                        if (!store.isPointDisabled({x, y})) {
                            moveTruckToPoint(level, snowplow.current, {x, y}, store.position);
                            store.updatePathLength({x, y}, store.position);
                            store.updateTraveledPoints({x, y}, store.position);
                            store.addPoint({x, y});
                            store.checkIsSolved();
                            store.checkIsFinished();
                            store.setPosition({x, y});
                        }
                    }}
                    getIsPointDisabled={(point: Coord) => store.isPointDisabled(point)}
                    coordinatesVisible={isCoordVisible}
                    level={store.level}
                >
                    <img
                        className="snowplow"
                        src="snowplow.svg"
                        height={PLOW_TRUCK_SIZE}
                        ref={snowplow}
                    />
                </Points>
            </div>
            <div className="buttonsGroup">
                <Button
                    variant="info"
                    onClick={() => {
                        if (isCoordVisible) {
                            setCoordsVisible(false);
                        } else {
                            setCoordsVisible(true);
                        }
                    }}
                >
                    {isCoordVisible ? 'Hide coordinates' : 'Show coordinates'}
                </Button>{' '}
                <Button
                    variant="secondary"
                    onClick={() => {
                        store.reset();
                        moveTruckToPoint(level, snowplow.current, toJS(store.position), null, 0);

                        // clear canvas
                        const canvas = canvasRef.current;
                        const context = canvas.getContext('2d');
                        context.clearRect(0, 0, canvas.width, canvas.height);
                    }}
                >
                    Reset
                </Button>
            </div>
            <CongratulationsModal
                showModal={showSuccessModal}
                onCloseModal={() => closeSuccessModal()}
            />
            <FailureModal
                showModal={showFailureModal}
                onCloseModal={() => closeFailureModal()}
                text={`Your route is ${store.pathDelta}% longer than the optimal one. Please try again.`}
            />
        </div>
    );
});

export default () => <GameView store={store} />;
