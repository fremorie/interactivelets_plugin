import {makeObservable, observable, action} from 'mobx';

import {Coord} from './types';
import {LEVELS} from './constants';
import {roundFloat} from './utils';
import core from "../../legacy/js/core";
import initCoursera from "../../legacy/js/init";

export class Store {
    isSolved = false;
    isFinished = false;
    path = 0;
    traveledPoints: Coord[][] = [];
    level: number | null = null;
    points: Coord[] = [];
    pathDelta = 0;
    position = {x: 0, y: 0};
    isRestored = false;

    constructor() {
        makeObservable(this, {
            level: observable,
            isSolved: observable,
            path: observable,
            traveledPoints: observable,
            points: observable,
            pathDelta: observable,
            position: observable,
            isRestored: observable,

            updatePathLength: action,
            checkIsSolved: action,
            updateTraveledPoints: action,
            addPoint: action,
            isPointDisabled: action,
            handleSuccess: action,
            checkIsFinished: action,
            init: action,
            setPosition: action,
            reset: action,
        });
    }

    updatePathLength(pointA: Coord, pointB: Coord) {
        const path = Math.sqrt((pointA.x - pointB.x) ** 2 + (pointA.y - pointB.y) ** 2);
        this.path = this.path + path;
    }

    updateTraveledPoints(pointA: Coord, pointB: Coord) {
        const points = [pointA, pointB];

        if (this.traveledPoints.includes(points) || this.traveledPoints.includes([pointB, pointA])) {
            return;
        }

        this.traveledPoints.push(points);
    }

    addPoint(point: Coord) {
        if (!this.points.find(p => p.x === point.x && point.y === point.y)) {
            this.points.push(point);
        }
    }

    checkIsSolved() {
        this.isSolved = roundFloat(this.path) === roundFloat(LEVELS[this.level].answer);
    }

    isPointDisabled(point: Coord) {
        return Boolean(this.points.find(p => p.x === point.x && point.y === point.y));
    }

    setPosition(point: Coord) {
        this.position = point;
        this.isRestored = false;
    }

    init() {
        // @ts-ignore
        courseraApi.callMethod({
            type: 'GET_SESSION_CONFIGURATION',
            onSuccess: (config: any) => {
                const {level, isSubmitted} = config;
                this.level = level;
                this.position = LEVELS[level].startingPoint;
                const isPreviouslyCompleted = config[`isPuzzlePreviouslyCompleted${level === undefined ? '' : `-${level}`}`];

                core({level}, isSubmitted);
                initCoursera({height: LEVELS[level].height, level, isPreviouslyCompleted}, isSubmitted);
            },
            onError: (error: any) => {
                console.error(`Failed to get session configuration, traceback: ${error}`)
            },
        });
    }

    reset() {
        this.points = [];
        this.path = 0;
        this.position = LEVELS[this.level].startingPoint;
        this.traveledPoints = [];
        this.isSolved = false;
        this.isFinished = false;
        this.isRestored = true;
    }

    handleSuccess() {
        // send successful event to coursera
        // @ts-ignore
        courseraApi.callMethod({
            type: "SET_ANSWER",
            data: {
                answer: {isCompleted: true}
            },
            onSuccess: () => {
                // make puzzle background green
                $('body').addClass('isCorrect');
            },
            onError: () => {
                console.error('Failed to set answer');
            },
        });

        const storedKey = `isPuzzlePreviouslyCompleted${this.level !== undefined ? `-${this.level}` : ''}`;

        // @ts-ignore
        courseraApi.callMethod({
            type: 'SET_STORED_VALUES',
            data: {
                [storedKey]: 'true',
            },
            onSuccess: () => {},
            onError: () => {
                console.error('Failed to set stored values');
            },
        });
    }

    handleFailure() {
    }

    checkIsFinished() {
        this.isFinished = this.isPointDisabled(LEVELS[this.level].startingPoint);

        if (this.isFinished) {
            if (this.isSolved) {
                this.handleSuccess();
            } else {
                this.pathDelta = (this.path - LEVELS[this.level].answer) / LEVELS[this.level].answer * 100;
                this.handleFailure();
            }
        }
    }
}
