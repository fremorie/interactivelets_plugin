const radToDegrees = (angle: number) => angle * (180 / Math.PI);

export const roundFloat = (num: number): number => Number.parseFloat(num.toFixed(10));

export const getAngleBetweenVectors = (pointA: {x: number, y: number}, pointB: {x: number, y: number}) => {
    const deltaY = pointB.y - pointA.y;
    const deltaX = pointB.x - pointA.x;

    const angle = Math.atan2(deltaY, deltaX);

    return radToDegrees(angle + Math.PI);
};
