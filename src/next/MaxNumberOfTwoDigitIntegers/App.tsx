import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import {Container, Button} from 'react-bootstrap';

import {addNumber, reset, removeNumber, closeModal} from './actions';

import {RANGES} from './constants';

import {NumbersRow} from './components/Row';
import {Selection} from './components/Selection';
import {CongratulationsModal} from '../common/components/CongratulationsModal';

type Props = {
    addNumber: (param: number) => void,
    disabledNumbers: number[],
    selectedNumbers: number[],
    reset: () => void,
    showModal: boolean,
    closeModal: () => void,
    removeNumber: (payload: number) => void,
};

const App = ({addNumber, disabledNumbers, selectedNumbers, reset, removeNumber, showModal, closeModal}: Props) => {
    return (
        <Container className="app">
            {RANGES.map((range, rowIndex) => (
                <NumbersRow
                    from={range[0]}
                    to={range[1]}
                    addNumber={addNumber}
                    removeNumber={removeNumber}
                    disabledNumbers={disabledNumbers}
                    selectedNumbers={selectedNumbers}
                    rowEventKey={rowIndex.toString()}
                />
            ))}
            <Selection selectionSize={selectedNumbers.length} />
            <Button variant="primary" onClick={reset}>Reset</Button>
            <CongratulationsModal showModal={showModal} onCloseModal={closeModal} />
        </Container>
    );
};

type State = {
    disabledNumbers: number[],
    selectedNumbers: number[],
    isSolved: boolean,
    showModal: boolean,
}

const mapStateToProps = (state: State) => ({
    disabledNumbers: state.disabledNumbers,
    selectedNumbers: state.selectedNumbers,
    isSolved: state.isSolved,
    showModal: state.showModal,
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
    addNumber,
    removeNumber,
    reset,
    closeModal,
}, dispatch);

const enhance = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default enhance(App);
