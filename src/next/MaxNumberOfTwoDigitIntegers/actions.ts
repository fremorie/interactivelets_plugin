import {CORRECT_TOTAL} from './constants';
import {setComplete} from '../common/utils/courseraApiHelpers';

export const ADD_NUMBER = 'game/ADD_NUMBER';
export const REMOVE_NUMBER = 'game/REMOVE_NUMBER';
export const RESET = 'game/RESET';
export const CLOSE_MODAL = 'game/CLOSE_MODAL';

export const ACTIONS = Object.freeze({
    ADD_NUMBER,
    RESET,
    REMOVE_NUMBER,
    CLOSE_MODAL,
});

type Actions = 'game/ADD_NUMBER' | 'game/RESET' | 'game/REMOVE_NUMBER' | 'game/CLOSE_MODAL';

export type DefaultAction = {
    type: Actions,
    payload: any,
};

export const addNumberDummy = (payload: number): DefaultAction => ({
    type: ADD_NUMBER,
    payload,
});

export const reset = (payload?: number): DefaultAction => ({
    type: RESET,
    payload,
});

export const removeNumber = (payload: number): DefaultAction => ({
    type: REMOVE_NUMBER,
    payload,
});

export const closeModal = (payload?: number): DefaultAction => ({
    type: CLOSE_MODAL,
    payload,
});

export const addNumber = (payload: number) => {
    return async function(dispatch: any, getState: any) {
        dispatch(addNumberDummy(payload));

        const state = getState();

        const isSolved = state.selectedNumbers.length + 1 === CORRECT_TOTAL;

        if (isSolved) {
            await setComplete();
        }
    };
 };
