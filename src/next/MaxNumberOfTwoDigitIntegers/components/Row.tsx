// @flow

import * as React from 'react';

import {Row, Col, Card} from 'react-bootstrap';

type Props = {
    from: number,
    to: number,
    addNumber: (param: number) => void,
    removeNumber: (param: number) => void,
    disabledNumbers: number[],
    selectedNumbers: number[],
    rowEventKey: string,
};

export const NumbersRow = ({from, to, addNumber, disabledNumbers, selectedNumbers, removeNumber}: Props) => {
    return (
        <Row>
            {Array.from({length: to - from + 1}, (v, k) => k + from).map(num => {
                const isDisabled = disabledNumbers.includes(num);
                const isSelected = selectedNumbers.includes(num);
                let backgroundColor;
                if (isDisabled) {
                    backgroundColor = 'danger';
                }
                if (isSelected) {
                    backgroundColor = 'success';
                }

                return (
                    <Col
                        xs={4}
                        sm={2}
                        md={true}
                        className={`rowNumber ${isDisabled ? 'disabled' : ''}`}
                        id={`${num}`}
                        onClick={() => {
                            if (!isDisabled && !isSelected) {
                                addNumber(num);
                            }
                            if (isSelected) {
                                removeNumber(num);
                            }
                        }}
                        key={num}
                    >
                        <Card bg={backgroundColor}>
                            {num}
                        </Card>
                    </Col>
                );
            })}
        </Row>
    )
};
