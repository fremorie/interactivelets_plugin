import * as React from 'react';

type Props = {
    selectionSize: number,
};

export const Selection = ({selectionSize}: Props) => {

    return (
        <div className="selection">
           Cards selected: {selectionSize}
        </div>
    )
};
