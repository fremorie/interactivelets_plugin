// @flow

import {InitialState} from './types';

export const INITIAL_STATE: InitialState = {
    selectedNumbers: [],
    disabledNumbers: [],
};


const GRID_SIZE = 99;
const WIDTH = 6;

const getRanges = () => {
    const ranges = [];

    for (let i = 10; i <= GRID_SIZE; i += WIDTH) {
        const range = [i, i + 5];
        ranges.push(range);
    }

    return ranges;
};

export const RANGES = getRanges();

export const CORRECT_TOTAL = 50;
