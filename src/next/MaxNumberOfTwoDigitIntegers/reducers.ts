import {ACTIONS, DefaultAction} from './actions';
import {InitialState} from './types';

import {getComplementaryNumber} from './utils';
import {CORRECT_TOTAL} from './constants';

export default (state: InitialState, action: DefaultAction) => {
    switch (action.type) {
        case ACTIONS.ADD_NUMBER:
            const selectedNumbers = [...state.selectedNumbers, action.payload];

            const isSolved = selectedNumbers.length === CORRECT_TOTAL;

            return {
                ...state,
                selectedNumbers,
                disabledNumbers: [...state.disabledNumbers, getComplementaryNumber(action.payload)],
                isSolved,
                showModal: isSolved,
            };

        case ACTIONS.RESET:
            return {
                // @ts-ignore
                selectedNumbers: [],
                // @ts-ignore
                disabledNumbers: [],
                isSolved: false,
            };
        case ACTIONS.REMOVE_NUMBER:
            const complementaryNumber = getComplementaryNumber(action.payload);

            return {
                ...state,
                selectedNumbers: state.selectedNumbers.filter((num: number) => num !== action.payload),
                disabledNumbers: state.disabledNumbers.filter((num: number) => num !== complementaryNumber),
            };
        case ACTIONS.CLOSE_MODAL:
            return {
                ...state,
                showModal: false,
            };
        default:
            return state;
    }
};
