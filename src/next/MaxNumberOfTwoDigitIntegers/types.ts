export type InitialState = {
    selectedNumbers: number[],
    disabledNumbers: number[],
};
