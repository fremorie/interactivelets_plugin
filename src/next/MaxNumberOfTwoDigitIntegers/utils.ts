// @flow

export const getComplementaryNumber = (num: number) => 100 - num;
