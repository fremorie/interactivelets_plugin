export const MOVE_TRUCK = 'game/PlowTruck/MOVE_TRUCK';
export const RESET = 'game/PlowTruck/RESET';

export const ACTIONS = Object.freeze({
    MOVE_TRUCK,
    RESET,
});

type Actions = typeof ACTIONS.MOVE_TRUCK | typeof ACTIONS.RESET;

export type DefaultAction = {
    type: Actions,
    payload?: any,
};

export const moveTruck = (payload: {newPosition: number[]}): DefaultAction => ({
    type: MOVE_TRUCK,
    payload,
});

export const reset = (): DefaultAction => ({
    type: RESET,
});
