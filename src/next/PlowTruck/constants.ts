import {State} from './types';
import {getVerticesCount} from './utils';

export const LEVELS = [
    {size: 2, answer: 16, height: 450},
    {size: 3, answer: 28, height: 500},
    {size: 4, answer: 48, height: 600},
    {size: 5, answer: 68, height: 700}
].map(x => ({...x, verticesCount: getVerticesCount(x.size)}));

const GRID_SIZE = 2;

export const INITIAL_STATE: State = {
    streetsCleaned: [],
    currentPosition: [GRID_SIZE, GRID_SIZE],
    direction: 'left',
    streetsDict: [],
    verticesCount: 0,
};
