import {SET_LEVEL, SET_COMPLETE} from '../common/actions/courseraActions';
import {onlyUnique} from '../common/utils/arrays';

import {ACTIONS, DefaultAction} from './actions';
import {State} from './types';
import {getDirection, getCleanedStreet} from './utils';
import {LEVELS} from './constants';

export default (state: State, action: DefaultAction) => {
    switch (action.type) {
        case SET_LEVEL:
            if (!action.payload) {
                const defaultLevel = 3;
                const level = LEVELS[defaultLevel];

                return {
                    ...state,
                    level: defaultLevel,
                    currentPosition: [level.size, level.size],
                    minPathLength: LEVELS[action.payload.level].answer,
                    gridSize: level.size,
                    verticesCount: level.verticesCount,
                }
            }

            const level = LEVELS[action.payload.level];

            return {
                ...state,
                level: action.payload.level,
                currentPosition: [level.size, level.size],
                minPathLength: LEVELS[action.payload.level].answer,
                gridSize: level.size,
                verticesCount: level.verticesCount,
            };

        case ACTIONS.MOVE_TRUCK:
            const start = state.currentPosition;
            const finish = action.payload.newPosition;

            const streetKey = [start, finish].map(coords => coords.join('-')).sort().join(' -> ');

            const direction = getDirection({start, finish});
            const cleanedStreet = getCleanedStreet({start, finish, gridSize: state.gridSize});

            return {
                ...state,
                currentPosition: action.payload.newPosition,
                direction,
                streetsCleaned: [...state.streetsCleaned, ...cleanedStreet],
                streetsDict: [...state.streetsDict, streetKey].filter(onlyUnique),
            };

        case ACTIONS.RESET:
            return {
                ...state,
                currentPosition: [state.gridSize, state.gridSize],
                direction: 'left',
                streetsCleaned: [],
                streetsDict: [],
            };

        default:
            return state;
    }
};
