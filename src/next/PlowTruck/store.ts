import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import reducers from './reducers';
import {INITIAL_STATE} from './constants';
import {State} from './types';

const composeEnhancers =
    typeof window === 'object' &&
        // @ts-ignore
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        // @ts-ignore
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        }) : compose;

const enhancer = composeEnhancers(
    applyMiddleware(thunk),
);

export default function configureStore(initialState: State = INITIAL_STATE) {
    return createStore(
        reducers,
        initialState,
        enhancer,
    );
}
