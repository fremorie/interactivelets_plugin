export type Direction = 'up' | 'down' | 'left' | 'right';
export type Border = 'top' | 'right' | 'bottom' | 'left';

export type State = {
    streetsCleaned: {square: number[], border: Border}[],
    currentPosition: number[],
    direction?: Direction,
    level?: number,
    gridSize?: number,
    minPathLength?: number,
    streetsDict?: string[],
    verticesCount: number,
};
