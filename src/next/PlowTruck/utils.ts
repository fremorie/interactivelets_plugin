export const getIsCoordValid = ({dot, gridSize}: {dot: number, gridSize: number}) => {
    return dot >= 0 && dot <= gridSize;
};

export const getAvailableDestinations = ({start, gridSize}: {start: number[], gridSize: number}) => {
    const availableDots = [];
    const [x, y] = start;

    for (let dx = -1; dx < 2; dx += 2) {
        const neighbourX = x + dx;
        const neighbourY = y;
        if (getIsCoordValid({dot: neighbourX, gridSize})) {
            availableDots.push([neighbourX, neighbourY]);
        }
    }

    for (let dy = -1; dy < 2; dy += 2) {
        const neighbourY = y + dy;
        const neighbourX = x;
        if (getIsCoordValid({dot: neighbourY, gridSize})) {
            availableDots.push([neighbourX, neighbourY]);
        }
    }

    return availableDots;
};

export const getDirection = ({start, finish}: {start: number[], finish: number[]}) => {
    const [x, y] = start;
    const [dx, dy] = finish;

    if (x === dx && dy < y) {
      return 'up';
    }
    if (x === dx && dy > y) {
      return 'down';
    }
    if (y === dy && dx < x) {
      return 'left';
    }
    if (y === dy && dx > x) {
      return 'right';
    }

    throw new Error('impossible coordinates');
};

export const getCleanedStreet = (
    {
        start,
        finish,
        gridSize
    }: {
        start: number[],
        finish: number[],
        gridSize: number,
    }) => {
    const [x, y] = start;
    let squares;
    const direction = getDirection({start, finish});

    switch (direction) {
        case 'left':
            squares = [{square: [x - 1, y - 1], border: 'bottom'}, {square: [x - 1, y], border: 'top'}];
            break;
        case 'right':
            squares = [{square: [x, y - 1], border: 'bottom'}, {square: [x, y], border: 'top'}];
            break;
        case 'down':
            squares = [{square: [x - 1, y], border: 'right'}, {square: [x, y], border: 'left'}];
            break;
        case 'up':
            squares = [{square: [x - 1, y - 1], border: 'right'}, {square: [x, y - 1], border: 'left'}];
            break;
        default:
            throw new Error('impossible direction');
    }

    const filteredSquares = squares.filter(el =>
        getIsCoordValid({dot: el.square[0], gridSize: gridSize - 1}) &&
        getIsCoordValid({dot: el.square[1], gridSize: gridSize - 1})
    );

    if (filteredSquares.length === 1) {
        return [{...filteredSquares[0], doubleBorder: true}];
    }

    return filteredSquares;
};

export const searchForArray = (haystack: (number[])[], needle: number[]) => {
    var i, j, current;
    for(i = 0; i < haystack.length; ++i){
        if(needle.length === haystack[i].length){
            current = haystack[i];
            for (j = 0; j < needle.length && needle[j] === current[j]; ++j);
            if (j === needle.length)
                return i;
        }
    }
    return -1;
};

export const getVerticesCount = (gridSize: number) => 2 * gridSize * (gridSize + 1);

export const getDirectionsWithCoords = (availableDots: (number[])[], currentPosition: number[]) => {
    const directions: {[key: string]: number[]} = {};
    for (let i = 0; i < availableDots.length; i++) {
        const dotDirection = getDirection({start: currentPosition, finish: availableDots[i]});
        directions[dotDirection.toUpperCase()] = availableDots[i];
    }

    return directions;
};
