import React from 'react'
import {observer} from 'mobx-react';
import {Jumbotron, Container, Tabs, Tab} from 'react-bootstrap';

// components
import Game from './Game';

// utils
import {range} from '../../common/utils/arrays';

import {ClientOnlyStore} from './clientOnlyStore';

type Props = {
    store: ClientOnlyStore,
};

const App = observer(({store}: Props) => {
    return (
        <Container fluid className="customFluidContainer">
            <Jumbotron>
                <Container>
                    <h1>Delivery Problem</h1>
                    <p>Visit all the points (and get back to the initial location) by a shortest route.</p>
                </Container>
            </Jumbotron>
            <Container>
                <Tabs defaultActiveKey="level0" id="uncontrolled-tab-example">
                    {range(Object.keys(store.progress || {}).length).map(level => (
                        <Tab
                            eventKey={`level${level}`}
                            title={`Level ${level + 1}`}
                            // @ts-ignore
                            tabClassName={store.progress[level].isCompleted ? 'levelCompleted' : ''}
                        >
                            <Game level={level} clientStore={store} />
                        </Tab>
                    ))}
                </Tabs>
            </Container>

            <div className="footer">
                <Container>
                    These interactive puzzles are part of two specializations at Coursera: <a
                    href="http://bit.ly/2B6a3Nn">Introduction to Discrete Mathematics for Computer Science</a> and <a
                    href="http://bit.ly/2AYsLpZ">Data Structures and Algorithms</a>. They provide you with a fun way to
                    "invent" the key ideas on your own! Even if you fail to solve some puzzles, the time will not be lost as
                    you will better appreciate the beauty and power of the underlying ideas. Besides more than 50 such
                    puzzles, the specializations contain more than 100 programming challenges. We encourage you to sign up
                    for a session and learn this material while interacting with thousands of other talented students from
                    around the world.
                </Container>
            </div>
        </Container>
    );
});

const store = new ClientOnlyStore();

export default () => <App store={store} />
