import React from 'react';
import {toJS} from 'mobx';
import gsap from 'gsap';
import {Button} from 'react-bootstrap';
import {observer} from "mobx-react"

// types
import {State, Direction, Border, Coord} from './types';

// styles & images
import './styles/index.scss';
import './images/snowplow.svg';

// constants
import {LEVELS, GRID_SIZE, PLOW_TRUCK_SIZE} from './constants';
import {Store} from './store';

// utils
import {getAngleBetweenVectors, getCanvasSize, normalizeCoords} from './utils';

// components
import {CongratulationsModal} from '../../common/components/CongratulationsModal';
import {FailureModal} from '../../common/components/FailureModal';
import {ClientOnlyStore} from "./clientOnlyStore";

const OFFSET = 50;

const moveTruckToPoint = (level: number, elem: HTMLImageElement, targetPoint: Coord, prevPoint?: Coord, duration?: number) => {
    let rotation = 0;

    if (prevPoint) {
        const processedPrevPoint = {
            x: getCanvasSize(LEVELS[level].normalizedCoords).width - prevPoint.x - OFFSET,
            // @ts-ignore
            y: prevPoint.y + OFFSET,
        };

        const processedTargetPoint = {
            x: getCanvasSize(LEVELS[level].normalizedCoords).width - targetPoint.x - OFFSET,
            // @ts-ignore
            y: targetPoint.y + OFFSET,
        }

        rotation = getAngleBetweenVectors(processedPrevPoint, processedTargetPoint);
    }

    gsap.to(elem, {
        duration: duration ? duration : 0.5,
        rotation: `${rotation}_short`,
    })
    gsap.to(elem, {
        delay: 0.5,
        x: getCanvasSize(LEVELS[level].normalizedCoords).width - targetPoint.x * GRID_SIZE - PLOW_TRUCK_SIZE / 3 - OFFSET,
        // @ts-ignore
        y: targetPoint.y * GRID_SIZE - PLOW_TRUCK_SIZE / 3 + OFFSET,
        duration: duration ? duration : 0.5,
    });
};

const CANVAS_HEIGHT = {
    0: 300,
    1: 600,
    2: 500,
    3: 600,
    4: 800,
};

const CANVAS_WIDTH = {
    0: 300,
    1: 600,
    2: 600,
    3: 500,
    4: 600,
};

type PointProps = {
    x: number,
    y: number,
    handleClick: () => void,
    isDisabled: boolean,
    coordinatesVisible: boolean,
    level: number,
    realCoords: Coord,
}

const Point = observer(({x, y, handleClick, isDisabled, coordinatesVisible, level, realCoords}: PointProps) => {
    return (
        <>
            <div
                className={`point ${isDisabled ? 'pointDisabled' : ''}`}
                onClick={() => handleClick()}
                // @ts-ignore
                style={{top: y * GRID_SIZE + OFFSET, left: getCanvasSize(LEVELS[level].normalizedCoords).width - x * GRID_SIZE - OFFSET}}
            />
            {coordinatesVisible && (
                <div
                    className="pointLegend"
                    // @ts-ignore
                    style={{top: y * GRID_SIZE + 15 + OFFSET, left: getCanvasSize(LEVELS[level].normalizedCoords).width - x * GRID_SIZE - OFFSET}}
                >
                    ({realCoords.x}, {realCoords.y})
                </div>
            )}
        </>
    );
});

const Points = observer(({coords, handlePointClick, children, getIsPointDisabled, coordinatesVisible, level}: {
    coords: number[][],
    handlePointClick: (p: { x: number; y: number }) => void,
    children: React.ReactNode,
    getIsPointDisabled: (point: Coord) => boolean,
    coordinatesVisible: boolean,
    level: number,
}) => {
    const realCoords = LEVELS[level].points;
    return (
        <div className="pointsContainer" style={{
            width: `${getCanvasSize(LEVELS[level].normalizedCoords).width}px`,
            height:`${getCanvasSize(LEVELS[level].normalizedCoords).height}px`,
        }}>
            {coords.map((point, index) => {
                const [x, y] = point;
                const [xR, yR] = realCoords[index];

                return (
                    <Point
                        key={`${x}-${y}`}
                        x={x}
                        y={y}
                        handleClick={() => handlePointClick({x, y})}
                        isDisabled={getIsPointDisabled({x, y})}
                        level={level}
                        coordinatesVisible={coordinatesVisible}
                        realCoords={{x: xR, y: yR}}
                    />
                );
            })}
            {children}
        </div>
    );
});

const Canvas = ({canvasRef, from, to, level}: {canvasRef: any, from: Coord, to: Coord, level: number}) => {
    const canvas = canvasRef.current;

    const draw = () => {
        const ctx = canvas.getContext('2d');
        ctx.lineWidth = 3;
        ctx.beginPath();
        // @ts-ignore
        ctx.moveTo(getCanvasSize(LEVELS[level].normalizedCoords).width - from.x * GRID_SIZE - OFFSET, from.y * GRID_SIZE + OFFSET);
        // @ts-ignore
        ctx.lineTo(getCanvasSize(LEVELS[level].normalizedCoords).width - to.x * GRID_SIZE - OFFSET, to.y * GRID_SIZE + OFFSET);
        ctx.stroke();
    }

    if (from && to) {
        draw();
    }

    // @ts-ignore
    return <canvas width={`${getCanvasSize(LEVELS[level].normalizedCoords).width}px`} height={`${getCanvasSize(LEVELS[level].normalizedCoords).height}px`} id="canvas" ref={canvasRef} />
}

type Props = {
    store: Store,
    level: number,
    clientStore: ClientOnlyStore,
}

const GameView = observer(({level, store, clientStore}: Props) => {
    const snowplow = React.useRef(null);

    const [showSuccessModal, setShowSuccessModal] = React.useState(false);
    const [showFailureModal, setShowFailureModal] = React.useState(false);
    const [isCoordVisible, setCoordsVisible] = React.useState(false);

    const closeSuccessModal = () => setShowSuccessModal(false);
    const closeFailureModal = () => setShowFailureModal(false);

    const canvasRef = React.useRef(null)

    React.useEffect(() => {
       if (store.isFinished) {
           if (store.isSolved) {
               setShowSuccessModal(true);
               clientStore.setLevelComplete(level);
           } else {
               setShowFailureModal(true);
           }
       }
    }, [store.path]);

    React.useEffect(() => {
        if (level !== null) {
            moveTruckToPoint(level, snowplow.current, toJS(store.position), null, 0);
        }
    }, [level, store.isSolved, store.isFinished]);

    if (level === null) {
        return <></>
    }

    return (
        <div className={`container container-${level}`}>
            <div className="center">
                <Canvas
                    level={level}
                    from={store.position}
                    to={store.traveledPoints.length > 0 ? store.traveledPoints[store.traveledPoints.length - 1][1] : undefined}
                    canvasRef={canvasRef}
                />
                <Points
                    coords={LEVELS[level].normalizedCoords}
                    handlePointClick={({x, y}) => {
                        if (!store.isPointDisabled({x, y})) {
                            moveTruckToPoint(level, snowplow.current, {x, y}, store.position);
                            store.updatePathLength({x, y}, store.position);
                            store.updateTraveledPoints({x, y}, store.position);
                            store.addPoint({x, y});
                            store.checkIsSolved();
                            store.checkIsFinished();
                            store.setPosition({x, y});
                        }
                    }}
                    getIsPointDisabled={(point: Coord) => store.isPointDisabled(point)}
                    coordinatesVisible={isCoordVisible}
                    level={level}
                >
                    <img
                        className="snowplow"
                        src="snowplow.svg"
                        height={PLOW_TRUCK_SIZE}
                        ref={snowplow}
                    />
                </Points>
            </div>
            <div className="buttonsGroup">
                <Button
                    variant="info"
                    onClick={() => {
                        if (isCoordVisible) {
                            setCoordsVisible(false);
                        } else {
                            setCoordsVisible(true);
                        }
                    }}
                >
                    {isCoordVisible ? 'Hide coordinates' : 'Show coordinates'}
                </Button>{' '}
                <Button
                    variant="secondary"
                    onClick={() => {
                        store.reset();
                        moveTruckToPoint(level, snowplow.current, toJS(store.position), null, 0);

                        // clear canvas
                        const canvas = canvasRef.current;
                        const context = canvas.getContext('2d');
                        context.clearRect(0, 0, canvas.width, canvas.height);
                    }}
                >
                    Reset
                </Button>
            </div>
            <CongratulationsModal
                showModal={showSuccessModal}
                onCloseModal={() => closeSuccessModal()}
            />
            <FailureModal
                showModal={showFailureModal}
                onCloseModal={() => closeFailureModal()}
                text={store.traveledPoints.length < LEVELS[level].points.length
                    ? 'Your route is missing some of the points.'
                    : `Your route is ${store.pathDelta}% longer than the optimal one. Please try again.`
                }
            />
        </div>
    );
});

const Game = ({level, clientStore}: {level: number, clientStore: ClientOnlyStore}) => {
    const store = new Store(level);

    return <GameView store={store} level={level} clientStore={clientStore} />
};

export default Game;
