import {makeObservable, observable, action} from 'mobx';

import {Coord} from './types';
import {LEVELS} from './constants';
import {roundFloat} from './utils';

export class ClientOnlyStore {
    progress: {
        0: {isCompleted: false},
        1: {isCompleted: false},
        2: {isCompleted: false},
        3: {isCompleted: false},
        4: {isCompleted: false},
    };

    constructor() {
        makeObservable(this, {
            progress: observable,

            setLevelComplete: action,
        });

        this.progress = {
            0: {isCompleted: false},
            1: {isCompleted: false},
            2: {isCompleted: false},
            3: {isCompleted: false},
            4: {isCompleted: false},
        }
    }

    setLevelComplete(level: number) {
        // @ts-ignore
        this.progress[level].isCompleted = true;
    }
}
