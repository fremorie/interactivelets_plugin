import {State} from './types';
import {normalizeCoords} from './utils';

const LEVELS_DEFAULT = [
    {
        answer: 365.60679107473624,
        height: 420,
        points: [[0,0], [100,20], [90,90], [10,110]],
        plowTruckHeight: 50,
        startingPoint: {x: 100, y: 20},
    },
    {
        answer: 645.127999859187,
        height: 710,
        points: [[284, 87], [183, 254], [113, 185], [159, 38], [271, 257]],
        startingPoint: {x: 183, y: 254},
    },
    {
        answer: 479.45380077539727,
        height: 620,
        points: [[100,50], [174,25], [129,99], [125,60], [211,209], [156,82]],
        startingPoint: {x: 174, y: 25},
    },
    {
        answer: 639.9899271133553,
        height: 730,
        points: [[180,280],[100,200],[44,105],[137, 277],[53, 202],[93, 296],[156, 214],[187, 137],[147, 195]],
        startingPoint: {x: 100, y: 200},
    },
    {
        answer: 833.5766624966727,
        height: 730,
        points: [[88,106], [248, 67], [251, 24], [124, 221], [136, 148], [60, 188], [272, 99], [30, 107], [50,50], [250,200]],
        startingPoint: {x: 248, y: 67},
    },
];

export const LEVELS = LEVELS_DEFAULT.map(level => {
    const normalizedCoords = normalizeCoords(level.points);

    return {
        ...level,
        normalizedCoords,
        startingPoint: {x: normalizedCoords[2][0], y: normalizedCoords[2][1]},
    };
});

export const GRID_SIZE = 2;

export const PLOW_TRUCK_SIZE = 80;
