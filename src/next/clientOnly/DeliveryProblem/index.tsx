// @flow

import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/index.scss';
import App from './App';

import ReactDOM from "react-dom";

ReactDOM.render((
        <App />
), document.getElementById('root'));