import {makeObservable, observable, action} from 'mobx';

import {Coord} from './types';
import {LEVELS} from './constants';
import {roundFloat} from './utils';

export class Store {
    isSolved = false;
    isFinished = false;
    path = 0;
    traveledPoints: Coord[][] = [];
    points: Coord[] = [];
    pathDelta = 0;
    level = 0;
    position = {x: 0, y: 0};
    isRestored = false;

    constructor(level: number) {
        makeObservable(this, {
            isSolved: observable,
            path: observable,
            traveledPoints: observable,
            points: observable,
            pathDelta: observable,
            position: observable,
            isRestored: observable,

            updatePathLength: action,
            checkIsSolved: action,
            updateTraveledPoints: action,
            addPoint: action,
            isPointDisabled: action,
            handleSuccess: action,
            checkIsFinished: action,
            setPosition: action,
            reset: action,
        });

        this.level = level;
        this.position = LEVELS[this.level].startingPoint;
    }

    updatePathLength(pointA: Coord, pointB: Coord) {
        const path = Math.sqrt((pointA.x - pointB.x) ** 2 + (pointA.y - pointB.y) ** 2);
        this.path = this.path + path;
    }

    updateTraveledPoints(pointA: Coord, pointB: Coord) {
        const points = [pointA, pointB];

        if (this.traveledPoints.includes(points) || this.traveledPoints.includes([pointB, pointA])) {
            return;
        }

        this.traveledPoints.push(points);
    }

    addPoint(point: Coord) {
        if (!this.points.find(p => p.x === point.x && point.y === point.y)) {
            this.points.push(point);
        }
    }

    checkIsSolved() {
        this.isSolved = roundFloat(this.path) === roundFloat(LEVELS[this.level].answer);
    }

    isPointDisabled(point: Coord) {
        return Boolean(this.points.find(p => p.x === point.x && point.y === point.y));
    }

    setPosition(point: Coord) {
        this.position = point;
        this.isRestored = false;
    }

    reset() {
        this.points = [];
        this.path = 0;
        this.position = LEVELS[this.level].startingPoint;
        this.traveledPoints = [];
        this.isSolved = false;
        this.isFinished = false;
        this.isRestored = true;
    }

    handleSuccess() {}

    handleFailure() {}

    checkIsFinished() {
        this.isFinished = this.isPointDisabled(LEVELS[this.level].startingPoint);

        if (this.isFinished) {
            if (this.isSolved) {
                this.handleSuccess();
            } else {
                this.pathDelta = (this.path - LEVELS[this.level].answer) / LEVELS[this.level].answer * 100;
                this.pathDelta = Math.round(this.pathDelta * 100) / 100;
                this.handleFailure();
            }
        }
    }
}
