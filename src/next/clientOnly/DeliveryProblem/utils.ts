import {Coord} from './types';
import {GRID_SIZE} from './constants';

const radToDegrees = (angle: number) => angle * (180 / Math.PI);

export const roundFloat = (num: number): number => Number.parseFloat(num.toFixed(10));

export const getAngleBetweenVectors = (pointA: {x: number, y: number}, pointB: {x: number, y: number}) => {
    const deltaY = pointB.y - pointA.y;
    const deltaX = pointB.x - pointA.x;

    const angle = Math.atan2(deltaY, deltaX);

    return radToDegrees(angle + Math.PI);
};

export const getCanvasSize = (coords: number[][]) => {
    const xs = coords.map(coord => {
        const [x, _] = coord;

        return x;
    });

    const ys = coords.map(coord => {
        const [_, y] = coord;

        return y;
    });

    const maxX = Math.max.apply(Math, xs);
    const maxY = Math.max.apply(Math, ys);

    return {
        height: maxY * GRID_SIZE + 100,
        width: maxX * GRID_SIZE + 100,
    }
}

export const normalizeCoords = (coords: number[][]) => {
    const xs = coords.map(coord => {
        const [x, _] = coord;

        return x;
    });

    const ys = coords.map(coord => {
        const [_, y] = coord;

        return y;
    });

    const maxX = Math.max.apply(Math, xs);
    const maxY = Math.max.apply(Math, ys);

    return coords.map(coord => {
        const [x, y] = coord;

        return [-(x - maxX), -(y -maxY)];
    })
};