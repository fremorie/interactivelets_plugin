import React from 'react';
import {connect} from 'react-redux';
import {Jumbotron, Container, Tabs, Tab} from 'react-bootstrap';

// components
import Game from './Game';

// utils
import {range} from '../../common/utils/arrays';

// types
import {State} from './types';

type Props = {
    progressInfo: {
        [key: number]: {isCompleted: boolean},
    },
};

const App = ({progressInfo}: Props) => (
    <Container fluid className="customFluidContainer">
        <Jumbotron>
            <Container>
                <h1>Plow Truck</h1>
                <p>A plow truck needs to clean every street and get back to its original location. Help it find the shortest route for this.</p>
            </Container>
        </Jumbotron>
        <Container>
            <Tabs defaultActiveKey="level0" id="uncontrolled-tab-example">
                {range(Object.keys(progressInfo).length).map(level => (
                    <Tab
                        eventKey={`level${level}`}
                        title={`Level ${level + 1}`}
                        tabClassName={progressInfo[level].isCompleted ? 'levelCompleted' : ''}
                    >
                        <Game level={level} />
                    </Tab>
                ))}
            </Tabs>
        </Container>

        <div className="footer">
            <Container>
                These interactive puzzles are part of two specializations at Coursera: <a
                href="http://bit.ly/2B6a3Nn">Introduction to Discrete Mathematics for Computer Science</a> and <a
                href="http://bit.ly/2AYsLpZ">Data Structures and Algorithms</a>. They provide you with a fun way to
                "invent" the key ideas on your own! Even if you fail to solve some puzzles, the time will not be lost as
                you will better appreciate the beauty and power of the underlying ideas. Besides more than 50 such
                puzzles, the specializations contain more than 100 programming challenges. We encourage you to sign up
                for a session and learn this material while interacting with thousands of other talented students from
                around the world.
            </Container>
        </div>
    </Container>
);

const mapStateToProps = (state: State) => ({
    progressInfo: state.progress,
});

const enhance = connect(mapStateToProps);

export default enhance(App);
