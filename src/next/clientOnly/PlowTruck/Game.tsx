import React from 'react';
import {connect} from 'react-redux';
import {Button, Container} from 'react-bootstrap';

// types
import {State, Direction, Border} from './types';

// styles & images
import './styles/index.scss';
import './images/snowplow.svg';

// actions
import {moveTruck, reset, setLevelComplete} from './actions';

// constants
import {LEVELS} from './constants';
import {ARROW_KEYS, ArrowKey} from '../../common/constants/keys';

// utils
import {range} from '../../common/utils/arrays';
import {getAvailableDestinations, searchForArray, getDirectionsWithCoords} from './utils';

// components
import {CongratulationsModal} from '../../common/components/CongratulationsModal';
import {FailureModal} from '../../common/components/FailureModal';

// component own state
type AppState = {
    streetsCleanedNumber: number,
    showSuccessModal: boolean,
    showFailureModal: boolean,
    allStreetsCleaned: boolean,
};

type PropsFromState = {
    currentPosition: number[],
    direction: Direction,
    streetsCleaned: {square: number[], border: Border}[],
    minPathLength: number,
    streetsDict: string[],
    verticesCount: number,
    availableDots: (number[])[],
    gridSize: number,
}

type DispatchProps = {
    moveTruck: (payload: {level: number, newPosition: number[]}) => void,
    reset: (payload: {level: number}) => void,
    setLevelComplete: (payload: {level: number}) => void,
};

type EnhancedProps = {
    level: number,
}

type Props = PropsFromState & DispatchProps & EnhancedProps;

class Game extends React.Component<Props, AppState> {
    constructor(props: Props) {
        super(props);

        this.state = {
            streetsCleanedNumber: 0,
            showSuccessModal: false,
            showFailureModal: false,
            allStreetsCleaned: false,
        };

        this.handleReset = this.handleReset.bind(this);
        this.closeSuccessModal = this.closeSuccessModal.bind(this);
        this.closeFailureModal = this.closeFailureModal.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
    }

    componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<AppState>, snapshot?: any): void {
        if (!this.state.allStreetsCleaned) {
            this.checkForSuccess();
        }
    }

    handleSuccess() {
        this.props.setLevelComplete({level: this.props.level});
        this.setState(() => ({
            showSuccessModal: true,
            allStreetsCleaned: true,
        }));
    }

    handleFailure() {
        this.setState(() => ({
            showFailureModal: true,
            allStreetsCleaned: true,
        }));
    }

    handleKeyDown(event: KeyboardEvent) {
        if (event.repeat) {
            return;
        }

        // prevent page from scroll
        event.preventDefault();

        const directions = getDirectionsWithCoords(this.props.availableDots, this.props.currentPosition);

        if (Object.keys(directions).map((direction: ArrowKey) => ARROW_KEYS[direction]).indexOf(event.keyCode) !== -1) {
            switch(event.keyCode) {
                case ARROW_KEYS.UP:
                    this.handleDotClick({coords: directions.UP, isHidden: false});
                    break;
                case ARROW_KEYS.DOWN:
                    this.handleDotClick({coords: directions.DOWN, isHidden: false});
                    break;
                case ARROW_KEYS.LEFT:
                    this.handleDotClick({coords: directions.LEFT, isHidden: false});
                    break;
                case ARROW_KEYS.RIGHT:
                    this.handleDotClick({coords: directions.RIGHT, isHidden: false});
                    break;
                default:
                    break;
            }
        }
    }

    checkForSuccess() {
        if (
            // the shortest path was found
            this.state.streetsCleanedNumber === this.props.minPathLength &&
            // and all streets are covered
            this.props.streetsDict.length === this.props.verticesCount &&
            // and the truck came back to its original location
            this.props.currentPosition.join('-') === [this.props.gridSize, this.props.gridSize].join('-')
        ) {
            this.handleSuccess();
        }

        if (
            // all streets are covered
            this.props.streetsDict.length === this.props.verticesCount &&
            // and the truck came back to its original location
            this.props.currentPosition.join('-') === [this.props.gridSize, this.props.gridSize].join('-') &&
            // but path is longer than it could be
            this.state.streetsCleanedNumber > this.props.minPathLength
        ) {
            this.handleFailure();
        }
    }

    handleDotClick({coords, isHidden}: {coords: number[], isHidden: boolean}) {
        if (!isHidden) {
            this.props.moveTruck({level: this.props.level, newPosition: coords});
            this.setState(state => ({
                streetsCleanedNumber: state.streetsCleanedNumber + 1,
            }));
        }
    }

    handleReset() {
        this.props.reset({level: this.props.level});
        this.setState(() => ({
            streetsCleanedNumber: 0,
            allStreetsCleaned: false,
        }))
    }

    renderDot({isHidden, coords}: {isHidden: boolean, coords: number[]}) {
        return (
            <div
                className={`dot ${isHidden ? 'hidden' : ''} dot-${coords.join('-')}`}
                onClick={() => this.handleDotClick({coords, isHidden})}
            />
        );
    }

    renderSquare({coords, borders}: {coords: number[], borders: Border[]}) {
        return (
            <div
                className={`square square-${coords.join('-')} ${borders.map(border => `square-border-${border || 'default'}`).join(' ')}`}
            />
        );
    }

    closeSuccessModal() {
        this.setState(() => ({showSuccessModal: false}))
    }

    closeFailureModal() {
        this.setState(() => ({showFailureModal: false}))
    }

    render() {
        if (this.props.level === undefined) {
            return null;
        }

        return (
            <Container>
                <div
                    className={`custom-container container-${this.props.level}`}
                    tabIndex={0}
                    // @ts-ignore
                    onKeyDown={this.handleKeyDown}
                >
                    <div className="grid">
                        <div className="dots">
                            {range(this.props.gridSize + 1).map(y => (
                                <div className="dotRow">
                                    {range(this.props.gridSize + 1).map(x =>
                                        this.renderDot({
                                            coords: [x, y],
                                            isHidden: searchForArray(this.props.availableDots, [x, y]) === -1
                                        })
                                    )}
                                </div>
                            ))}
                        </div>
                        <div className="squares">
                            {range(this.props.gridSize).map(y => (
                                <div className="board-row">
                                    {range(this.props.gridSize).map(x => {
                                        const streets = this.props.streetsCleaned
                                            .filter(street => street?.square?.[0] === x && street?.square?.[1] === y);
                                        const borders = streets?.map(el => el?.border);

                                        return this.renderSquare({coords: [x, y], borders});
                                    })}
                                </div>
                            ))}
                        </div>
                        <img
                            className={`snowplow snowplow-${this.props.currentPosition.join('-')} snowplow-${this.props.direction}`}
                            src="snowplow.svg"
                            height={100}
                        />
                    </div>
                    <div className="unitsTraveled">
                        Units traveled: {this.state.streetsCleanedNumber}
                    </div>
                    <Button className="resetButton" onClick={this.handleReset} variant="info">
                        Reset
                    </Button>

                    <CongratulationsModal
                        showModal={this.state.showSuccessModal}
                        onCloseModal={() => this.closeSuccessModal()}
                    />
                    <FailureModal
                        showModal={this.state.showFailureModal}
                        onCloseModal={() => this.closeFailureModal()}
                        text={'You\'ve cleaned every street, but there is a shorter route. Try to find it!'}
                    />
                </div>
            </Container>
        );
    }
}

const mapStateToProps = (state: State, {level}: EnhancedProps): PropsFromState => {
    const levelState = state[level];

    return {
        currentPosition: levelState.currentPosition,
        direction: levelState.direction,
        streetsCleaned: levelState.streetsCleaned,
        streetsDict: levelState.streetsDict,
        minPathLength: levelState.minPathLength,
        verticesCount: levelState.verticesCount,
        gridSize: levelState.gridSize,
        availableDots: getAvailableDestinations({
            start: levelState.currentPosition,
            gridSize: levelState.gridSize,
        }),
    };
};

const mapDispatchToProps: DispatchProps = {moveTruck, reset, setLevelComplete};

const enhance = connect(mapStateToProps, mapDispatchToProps);

export default enhance(Game);
