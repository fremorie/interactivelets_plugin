export const MOVE_TRUCK = 'game/PlowTruck/MOVE_TRUCK';
export const RESET = 'game/PlowTruck/RESET';
export const SET_LEVEL_COMPLETE = 'game/PlowTruck/SET_LEVEL_COMPLETE';

export const ACTIONS = Object.freeze({
    MOVE_TRUCK,
    RESET,
    SET_LEVEL_COMPLETE,
});

type Actions = typeof ACTIONS.MOVE_TRUCK | typeof ACTIONS.RESET | typeof ACTIONS.SET_LEVEL_COMPLETE;

export type DefaultAction = {
    type: Actions,
    payload?: any,
};

export const moveTruck = (payload: {newPosition: number[]}): DefaultAction => ({
    type: MOVE_TRUCK,
    payload,
});

export const reset = (payload: {level: number}): DefaultAction => ({
    type: RESET,
    payload,
});

export const setLevelComplete = (payload: {level: number}): DefaultAction => ({
    type: SET_LEVEL_COMPLETE,
    payload,
});
