import {State} from './types';
import {getVerticesCount} from './utils';

export const LEVELS = [
    {size: 2, answer: 16, height: 400},
    {size: 3, answer: 28, height: 500},
    {size: 4, answer: 48, height: 600},
    {size: 5, answer: 68, height: 700}
].map(x => ({...x, verticesCount: getVerticesCount(x.size)}));

export const INITIAL_STATE: State = LEVELS.reduce((acc, levelData, index) => ({
    ...acc,
    [index]: {
        streetsCleaned: [],
        currentPosition: [LEVELS[index].size, LEVELS[index].size],
        direction: 'left',
        streetsDict: [],
        verticesCount: LEVELS[index].verticesCount,
        gridSize: LEVELS[index].size,
        minPathLength: LEVELS[index].answer,
    }
}), {
    progress: {
        0: {isCompleted: false},
        1: {isCompleted: false},
        2: {isCompleted: false},
        3: {isCompleted: false},
    }
});
