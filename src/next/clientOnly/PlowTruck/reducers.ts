import {onlyUnique} from '../../common/utils/arrays';

import {ACTIONS, DefaultAction} from './actions';
import {State} from './types';
import {getDirection, getCleanedStreet} from './utils';

export default (state: State, action: DefaultAction) => {
    switch (action.type) {
        case ACTIONS.MOVE_TRUCK:
            const start = state[action.payload.level].currentPosition;
            const finish = action.payload.newPosition;

            const streetKey = [start, finish].map(coords => coords.join('-')).sort().join(' -> ');

            const direction = getDirection({start, finish});
            const cleanedStreet = getCleanedStreet({start, finish, gridSize: state[action.payload.level].gridSize});

            return {
                ...state,
                [action.payload.level]: {
                    ...state[action.payload.level],
                    currentPosition: action.payload.newPosition,
                    direction,
                    streetsCleaned: [...state[action.payload.level].streetsCleaned, ...cleanedStreet],
                    streetsDict: [...state[action.payload.level].streetsDict, streetKey].filter(onlyUnique),
                },
            };

        case ACTIONS.RESET:
            return {
                ...state,
                [action.payload.level]: {
                    ...state[action.payload.level],
                    currentPosition: [state[action.payload.level].gridSize, state[action.payload.level].gridSize],
                    direction: 'left',
                    streetsCleaned: [],
                    streetsDict: [],
                },
            };

        case ACTIONS.SET_LEVEL_COMPLETE:
            return {
                ...state,
                progress: {
                    ...state.progress,
                    [action.payload.level]: {
                        isCompleted: true,
                    }
                }
            };

        default:
            return state;
    }
};
