export type Direction = 'up' | 'down' | 'left' | 'right';
export type Border = 'top' | 'right' | 'bottom' | 'left';

export type State = {
    [key: number]: {
        streetsCleaned: {square: number[], border: Border}[],
        currentPosition: number[],
        direction: Direction,
        gridSize: number,
        minPathLength?: number,
        streetsDict: string[],
        verticesCount: number,
    },
    progress: {
        [key: number]: {
            isCompleted: boolean,
        }
    }
};
