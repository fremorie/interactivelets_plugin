import {CourseraConfig, StoredValues} from "../types/courseraApiTypes";

export const SET_LEVEL = 'common/SET_LEVEL';
export const SET_COMPLETE = 'common/SET_COMPLETE';

export type DefaultAction = {
    type: string,
    payload?: any,
};

export const setLevel = (payload: {level: number}): DefaultAction => ({
    type: SET_LEVEL,
    payload,
});

export const setCompleteDummy = (): DefaultAction => ({
    type: SET_COMPLETE,
});

export const initCoursera = ({height, level}: {height?: number, level?: number}) => {
    return async (dispatch: Function) => {
        if (height !== undefined) {
            // @ts-ignore<courseraApi cannot be fetched other than by being placed in html head>
            await courseraApi.callMethod({
                type: "SET_HEIGHT",
                data: {
                    height: `${height}px`
                }
            });
        }

        const storedKey = `isPuzzlePreviouslyCompleted${level !== undefined ? `-${level}` : ''}`;

        // @ts-ignore<courseraApi cannot be fetched other than by being placed in html head>
        await courseraApi.callMethod({
            type: "GET_STORED_VALUES",
            data: [storedKey],
            onSuccess: function(values: StoredValues) {
                if (values[storedKey] === 'true') {
                    // make puzzle background green
                    $('body').addClass('isCorrect');

                    // @ts-ignore<courseraApi cannot be fetched other than by being placed in html head>
                    courseraApi.callMethod({
                        type: "SET_ANSWER",
                        data: {
                            answer: {isCompleted: true}
                        },
                        onSuccess: () => {},
                        onError: () => {
                            console.error('Failed to set answer');
                        },
                    });
                }
            },
            onError: function() {
                console.error('Failed to get stored values');
            }
        });
    };
};

export const getLevel = (levelSizeInfo: {[key: number]: {height: number}}) => {
    return async (dispatch: Function) => {
        // @ts-ignore
        await courseraApi.callMethod({
            type: 'GET_SESSION_CONFIGURATION',
            onSuccess: (config: CourseraConfig) => {
                const {level} = config;
                dispatch(setLevel({level}));
                dispatch(initCoursera({level, height: levelSizeInfo[level].height}));
            },
            onError: (error: any) => {
                console.error(`Failed to get session configuration, traceback: ${error}`)
            },
        });
    };
};

export const setComplete = (level?: number) => {
    return async (dispatch: Function) => {
        dispatch(setCompleteDummy());

        const storedValuesKey = level === undefined
            ? 'isPuzzlePreviouslyCompleted'
            : `isPuzzlePreviouslyCompleted-${level}`;

        // @ts-ignore<courseraApi cannot be fetched other than by being placed in html head>
        await courseraApi.callMethod({
            type: "SET_ANSWER",
            data: {
                answer: {isCompleted: true}
            },
            onSuccess: () => {
                // make puzzle background green
                $('body').addClass('isCorrect');
            },
            onError: () => {
                console.error('Failed to set answer');
            },
        });

        // @ts-ignore<courseraApi cannot be fetched other than by being placed in html head>
        await courseraApi.callMethod({
            type: 'SET_STORED_VALUES',
            data: {
                [storedValuesKey]: 'true',
            },
            onSuccess: () => {},
            onError: () => {
                console.error('Failed to set stored values');
            },
        });
    }
};
