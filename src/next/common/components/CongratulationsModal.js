"use strict";
exports.__esModule = true;
exports.CongratulationsModal = void 0;
var React = require("react");
var react_bootstrap_1 = require("react-bootstrap");
exports.CongratulationsModal = function (_a) {
    var showModal = _a.showModal, onCloseModal = _a.onCloseModal;
    return (React.createElement(react_bootstrap_1.Modal, { show: showModal, onHide: onCloseModal },
        React.createElement(react_bootstrap_1.Modal.Header, { closeButton: true },
            React.createElement(react_bootstrap_1.Modal.Title, null, "Congratulations! You're right!")),
        React.createElement(react_bootstrap_1.Modal.Footer, null,
            React.createElement(react_bootstrap_1.Button, { variant: "secondary", onClick: onCloseModal }, "Close"))));
};
