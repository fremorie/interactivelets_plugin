import * as React from 'react';

import {Button, Modal} from 'react-bootstrap';

type Props = {
    showModal: boolean,
    onCloseModal: () => void,
};

export const CongratulationsModal = ({showModal, onCloseModal}: Props) => {
    return (
        <Modal show={showModal} onHide={onCloseModal}>
            <Modal.Header closeButton>
                <Modal.Title>Congratulations! You're right!</Modal.Title>
            </Modal.Header>
            <Modal.Footer>
                <Button variant="secondary" onClick={onCloseModal}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    );
};
