import * as React from 'react';

import {Button, Modal} from 'react-bootstrap';

type Props = {
    showModal: boolean,
    onCloseModal: () => void,
    text: string,
};

export const FailureModal = ({showModal, onCloseModal, text}: Props) => {
    return (
        <Modal show={showModal} onHide={onCloseModal}>
            <Modal.Header closeButton>
                <Modal.Title>{text}</Modal.Title>
            </Modal.Header>
            <Modal.Footer>
                <Button variant="secondary" onClick={onCloseModal}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    );
};
