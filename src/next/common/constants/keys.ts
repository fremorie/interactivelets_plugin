export type ArrowKey = 'UP' | 'LEFT' | 'DOWN' | 'RIGHT';

export const ARROW_KEYS = {
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
};
