export type CourseraConfig = {
    level?: number,
}

export type StoredValues = {
    [key: string]: 'true' | 'false',
}
