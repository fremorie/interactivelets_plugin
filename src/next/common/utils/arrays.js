"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.onlyUnique = exports.range = void 0;
exports.range = function (n) { return __spreadArrays(Array(n)).map(function (x, index) { return index; }); };
exports.onlyUnique = function (value, index, self) { return self.indexOf(value) === index; };
