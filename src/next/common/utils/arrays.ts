export const range = (n: Number): number[] => [...Array(n)].map((x, index) => index);

export const onlyUnique = (value: any, index: number, self: Array<any>) => self.indexOf(value) === index;
