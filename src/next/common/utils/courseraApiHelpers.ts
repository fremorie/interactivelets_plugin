// informs Coursera that current plugin instance is completed
export const setComplete = async (level?: number) => {
    const storedValuesKey = level === undefined
        ? 'isPuzzlePreviouslyCompleted'
        : `isPuzzlePreviouslyCompleted-${level}`;

    // @ts-ignore<courseraApi cannot be fetched other than by being placed in html head>
    await courseraApi.callMethod({
        type: "SET_ANSWER",
        data: {
            answer: {isCompleted: true}
        },
        onSuccess: () => {
            // make puzzle background green
            $('body').addClass('isCorrect');
        },
        onError: () => {
            console.error('Failed to set answer');
        },
    });

    // @ts-ignore<courseraApi cannot be fetched other than by being placed in html head>
    await courseraApi.callMethod({
        type: 'SET_STORED_VALUES',
        data: {
            [storedValuesKey]: 'true',
        },
        onSuccess: () => {},
        onError: () => {
            console.error('Failed to set stored values');
        },
    });
};
