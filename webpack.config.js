const CopyWebpackPlugin = require('copy-webpack-plugin-advanced');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const getLevels = numberOfLevels => [...Array(numberOfLevels).keys()].map(x => x + 1);

const PUZZLES = {
    ['arthurs-books']: {
        isSwitchingConfigurations: true,
    },
    ['arthurs-books-solver']: {
        isSwitchingConfigurations: true,
    },
    ['arrange-apples']: {
        levels: getLevels(2),
        isSingleEntryPoint: true,
    },
    ['activity-selection']: {
        isSwitchingConfigurations: true,
    },
    ['antimagic-square']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['antivirus-system']: {
        isSwitchingConfigurations: true,
    },
    ['15-game']: {
        isSwitchingConfigurations: true,
    },
    ['road-repair']: {
        levels: getLevels(3),
        isSingleEntryPoint: false,
    },
    ['find-number']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['florin-coins']: {
        isSwitchingConfigurations: true,
    },
    ['magic-square']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['n-bishops']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['n-knights']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['n-rooks']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['numbers-in-boxes']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['numbers-on-chessboard']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['opposite-colors']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['prime']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['prisoner-and-king']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['sums-of-rows-and-columns']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['take-the-last-stone']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['three-rocks-game']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['twice-numbers']: {
        levels: getLevels(1),
        isSingleEntryPoint: true,
    },
    ['balanced-graphs']: {
        levels: getLevels(2),
        isSingleEntryPoint: false,
    },
    ['bridges']: {
        levels: getLevels(2),
        isSingleEntryPoint: false,
    },
    ['chessboard-tiling']: {
        isSwitchingConfigurations: true,
    },
    ['clock-game']: {
        isSwitchingConfigurations: true,
    },
    ['cut-the-figure']: {
        isSwitchingConfigurations: true,
    },
    ['find-number-2']: {
        levels: getLevels(4),
        isSingleEntryPoint: false,
    },
    ['graph-cliques']: {
        levels: getLevels(3),
        isSingleEntryPoint: false,
    },
    ['guarinis-puzzle']: {
        levels: getLevels(3),
        isSingleEntryPoint: false,
    },
    ['hamiltonian-cycle']: {
        levels: getLevels(3),
        isSingleEntryPoint: false,
    },
    ['hanoi-towers']: {
        isSwitchingConfigurations: true,
    },
    ['job-assignment']: {
        levels: getLevels(3),
        isSingleEntryPoint: false,
    },
    ['largest-number']: {
        levels: getLevels(4),
        isSingleEntryPoint: false,
    },
    ['local-maximum']: {
        isSwitchingConfigurations: true,
    },
    ['make-a-tree']: {
        levels: getLevels(3),
        isSingleEntryPoint: false,
    },
    ['car-fueling']: {
        levels: getLevels(5),
        isSingleEntryPoint: false,
    },
    ['map-coloring']: {
        levels: getLevels(3),
        isSingleEntryPoint: false,
    },
    ['n-diagonals']: {
        isSwitchingConfigurations: true,
    },
    ['n-queens']: {
        isSwitchingConfigurations: true,
    },
    ['number-of-paths']: {
        levels: getLevels(4),
        isSingleEntryPoint: false,
    },
    ['piece-on-chessboard']: {
        isSwitchingConfigurations: true,
    },
    ['regular-graph']: {
        isSwitchingConfigurations: true,
    },
    ['subway-lines']: {
        levels: getLevels(3),
        isSingleEntryPoint: false,
    },
    ['summing-up-binomials']: {
        levels: getLevels(4),
        isSingleEntryPoint: false,
    },
    ['summing-up-digits']: {
        isSwitchingConfigurations: true,
    },
    ['switching-signs']: {
        isSwitchingConfigurations: true,
    },
    ['take-the-last-rock']: {
        levels: getLevels(7),
        isSingleEntryPoint: false,
    },
    ['three-hotels']: {
        isSwitchingConfigurations: true,
    },
    ['tossing-coins']: {
        isSwitchingConfigurations: true,
    },
    ['touch-all-segments']: {
        levels: getLevels(3),
        isSingleEntryPoint: false,
    },
    ['balls-in-boxes']: {
        isSwitchingConfigurations: true,
    },
    ['maximum-advertisement-revenue']: {
        levels: getLevels(3),
        isSingleEntryPoint: true,
    },
    ['primitive-calculator']: {
        levels: getLevels(5),
        isSingleEntryPoint: true,
    },
    ['split-weights']: {
        isSwitchingConfigurations: true,
    },
};

const flatten = function(arr, result = []) {
    for (let i = 0, length = arr.length; i < length; i++) {
        const value = arr[i];
        if (Array.isArray(value)) {
            flatten(value, result);
        } else {
            result.push(value);
        }
    }
    return result;
};

const getSwitchingConfigurationsConfig = puzzleName => ({
    mode: 'development',
    entry: {
        [puzzleName]: `./src/legacy/js/${puzzleName}/js/commonApp.js`,
    },
    output: {
        filename: '[name]/main.js',
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        new HtmlWebpackPlugin({
            chunks: [puzzleName],
            template: `./src/legacy/html/${puzzleName}/index.html`,
            filename: `${puzzleName}/index.html`,
            inject: false,
            minify: puzzleName !== 'prime',
        }),
        new CopyWebpackPlugin([{
            from: `./src/legacy/static/manifests/${puzzleName}/manifest.json`,
            to: `${puzzleName}/manifest.json`,
        }]),
        new CopyWebpackPlugin([{
            from: './src/legacy/js/grader.js',
            to: `${puzzleName}/grader.js`,
        }]),
    ],
    module: {
        rules: [
            {
                test:    /\.js$/,
                exclude: /(node_modules)/,
                include: `${__dirname}/src`,
                use:     {
                    loader:  "babel-loader",
                },
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        query: {
                            name: `${puzzleName}/[name].[ext]`
                        }
                    },
                ],
            },
            {
                test: /\.css$/i,
                // loader: 'style!css',
                use: [
                    // The `injectType`  option can be avoided because it is default behaviour
                    {
                        loader: 'style-loader',
                        options: {
                            injectType: 'singletonStyleTag',
                        },
                    },
                    'css-loader',
                ],
            },
        ],
    },
});

const getSingleLevelConfig = puzzleName => ({
    mode: 'development',
    entry: {
        [puzzleName]: `./src/legacy/js/${puzzleName}/js/index.js`,
    },
    output: {
        filename: '[name]/main.js',
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        new HtmlWebpackPlugin({
            chunks: [puzzleName],
            template: `./src/legacy/html/${puzzleName}/index.html`,
            filename: `${puzzleName}/index.html`,
            inject: false,
            minify: puzzleName !== 'prime',
        }),
        new CopyWebpackPlugin([{
            from: `./src/legacy/static/manifests/${puzzleName}/manifest.json`,
            to: `${puzzleName}/manifest.json`,
        }]),
        new CopyWebpackPlugin([{
            from: './src/legacy/js/grader.js',
            to: `${puzzleName}/grader.js`,
        }]),
        // new CopyWebpackPlugin([{
        //     from: './src/static/css/common.css',
        //     to: `${puzzleName}/common.css`,
        // }]),
    ],
    module: {
        rules: [
            {
                test:    /\.js$/,
                exclude: /(node_modules)/,
                include: `${__dirname}/src`,
                use:     {
                    loader:  "babel-loader",
                },
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        query: {
                            name: `${puzzleName}/[name].[ext]`
                        }
                    },
                ],
            },
            {
                test: /\.css$/i,
                // loader: 'style!css',
                use: [
                    // The `injectType`  option can be avoided because it is default behaviour
                    {
                        loader: 'style-loader',
                        options: {
                            injectType: 'singletonStyleTag',
                        },
                    },
                    'css-loader',
                ],
            },
        ],
    },
});

const getSingleEntryPointConfig = (puzzleName, levels) => levels.map(level => ({
    mode: 'development',
    entry: {
        [puzzleName]: `./src/legacy/js/${puzzleName}/js/index.js`,
    },
    output: {
        filename: `[name]/[name]-level${level}/main.js`,
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        new HtmlWebpackPlugin({
            chunks: [puzzleName],
            template: `./src/legacy/html/${puzzleName}/${puzzleName}-level${level}/index.html`,
            filename: `${puzzleName}/${puzzleName}-level${level}/index.html`,
            inject: false,
            minify: puzzleName !== 'prime',
        }),
        new CopyWebpackPlugin([{
            from: `./src/legacy/static/manifests/${puzzleName}/${puzzleName}-level${level}/manifest.json`,
            to: `${puzzleName}/${puzzleName}-level${level}/manifest.json`,
        }]),
        new CopyWebpackPlugin([{
            from: './src/legacy/js/grader.js',
            to: `${puzzleName}/${puzzleName}-level${level}/grader.js`,
        }]),
        // new CopyWebpackPlugin([{
        //     from: './src/static/css/common.css',
        //     to: `${puzzleName}/${puzzleName}-level${level}/common.css`,
        // }]),
    ],
    module: {
        rules: [
            {
                test:    /\.js$/,
                exclude: /(node_modules)/,
                include: `${__dirname}/src`,
                use:     {
                    loader:  "babel-loader",
                },
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        query: {
                            name: `${puzzleName}/${puzzleName}-level${level}/[name].[ext]`
                        }
                    },
                ],
            },
            {
                test: /\.css$/i,
                // loader: 'style!css',
                use: [
                    // The `injectType`  option can be avoided because it is default behaviour
                    {
                        loader: 'style-loader',
                        options: {
                            injectType: 'singletonStyleTag',
                        },
                    },
                    'css-loader',
                ],
            },
        ],
    },
}));

const getMultiLevelConfig = (puzzleName, levels) => levels.map(level => ({
    mode: 'development',
    entry: {
        [puzzleName]: `./src/legacy/js/${puzzleName}/js/level${level}.js`,
    },
    output: {
        filename: `[name]/[name]-level${level}/main.js`,
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        new HtmlWebpackPlugin({
            chunks: [puzzleName],
            template: `./src/legacy/html/${puzzleName}/${puzzleName}-level${level}/index.html`,
            filename: `${puzzleName}/${puzzleName}-level${level}/index.html`,
            inject: false,
            minify: puzzleName !== 'prime',
        }),
        new CopyWebpackPlugin([{
            from: `./src/legacy/static/manifests/${puzzleName}/${puzzleName}-level${level}/manifest.json`,
            to: `${puzzleName}/${puzzleName}-level${level}/manifest.json`,
        }]),
        new CopyWebpackPlugin([{
            from: './src/legacy/js/grader.js',
            to: `${puzzleName}/${puzzleName}-level${level}/grader.js`,
        }]),
        // new CopyWebpackPlugin([{
        //     from: './src/static/css/common.css',
        //     to: `${puzzleName}/${puzzleName}-level${level}/common.css`,
        // }]),
    ],
    module: {
        rules: [
            {
                test:    /\.js$/,
                exclude: /(node_modules)/,
                include: `${__dirname}/src`,
                use:     {
                    loader:  "babel-loader",
                },
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        query: {
                            name: `${puzzleName}/${puzzleName}-level${level}/[name].[ext]`
                        }
                    },
                ],
            },
            {
                test: /\.css$/i,
                // loader: 'style!css',
                use: [
                    // The `injectType`  option can be avoided because it is default behaviour
                    {
                        loader: 'style-loader',
                        options: {
                            injectType: 'singletonStyleTag',
                        },
                    },
                    'css-loader',
                ],
            },
        ],
    },
}));

const configs = Object.keys(PUZZLES).map(puzzleName => {
    const puzzle = PUZZLES[puzzleName];

    if (puzzle.isSwitchingConfigurations) {
        return getSwitchingConfigurationsConfig(puzzleName);
    }

    if (puzzle.levels.length === 1) {
        return getSingleLevelConfig(puzzleName);
    }

    if (puzzle.isSingleEntryPoint) {
        return getSingleEntryPointConfig(puzzleName, puzzle.levels);
    }

    return getMultiLevelConfig(puzzleName, puzzle.levels);
});

const config = () => {
    console.log('Flattening configs...');
    return flatten(configs);
};

const REACT_PUZZLES = [
    'MaxNumberOfTwoDigitIntegers',
    'PlowTruck',
    'DeliveryProblem',
];

const clientOnlyConfig = puzzleName => ({
    entry: `./src/next/clientOnly/${puzzleName}/index.tsx`,
    output: {
        filename: `clientOnly/${puzzleName}/main.js`,
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        new HtmlWebpackPlugin({
            chunks: puzzleName,
            template: `./src/next/clientOnly/${puzzleName}/index.html`,
            filename: `clientOnly/${puzzleName}/index.html`,
            inject: false,
            minify: false,
        }),
    ],
    optimization: {
        // We do not want to minimize our code.
        minimize: false
    },
    module: {
        rules: [
            {
                // Uglify all files except for actions, since they include courseraAPI calls
                test: /.*\/actions\/.*\.js$/,
                exclude: /.spec.js/, // excluding .spec files
                loader: "uglify"
            },
            { test: /\.jsx?$/, loader: 'babel-loader', exclude: /node_modules/ },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    'style-loader',
                    // Translates CSS into CommonJS
                    'css-loader',
                    // Compiles Sass to CSS
                    'sass-loader',
                ],
            },
            { test: /\.css$/, use: [ 'style-loader', 'css-loader' ] },
            { test: /\.tsx?$/, use: ['babel-loader'] },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        query: {
                            name: `clientOnly/${puzzleName}/[name].[ext]`
                        }
                    },
                ],
            },
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ],
    },
});

const reactConfig = puzzleName => ({
    entry: `./src/next/${puzzleName}/index.tsx`,
    output: {
        filename: `${puzzleName}/main.js`,
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        new HtmlWebpackPlugin({
            chunks: puzzleName,
            template: `./src/next/${puzzleName}/index.html`,
            filename: `${puzzleName}/index.html`,
            inject: false,
            minify: false,
        }),
        new CopyWebpackPlugin([{
            from: `./src/next/${puzzleName}/manifest.json`,
            to: `${puzzleName}/manifest.json`,
        }]),
        new CopyWebpackPlugin([{
            from: './src/legacy/js/grader.js',
            to: `${puzzleName}/grader.js`,
        }]),
    ],
    optimization: {
        // We do not want to minimize our code.
        minimize: false
    },
    module: {
        rules: [
            {
                // Uglify all files except for actions, since they include courseraAPI calls
                test: /.*\/actions\/.*\.js$/,
                exclude: /.spec.js/, // excluding .spec files
                loader: "uglify"
            },
            { test: /\.jsx?$/, loader: 'babel-loader', exclude: /node_modules/ },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    'style-loader',
                    // Translates CSS into CommonJS
                    'css-loader',
                    // Compiles Sass to CSS
                    'sass-loader',
                ],
            },
            { test: /\.css$/, use: [ 'style-loader', 'css-loader' ] },
            { test: /\.tsx?$/, use: ['babel-loader'] },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        query: {
                            name: `${puzzleName}/[name].[ext]`
                        }
                    },
                ],
            },
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ],
    },
});

module.exports = [
    ...config(),
    ...REACT_PUZZLES.map(puzzle => reactConfig(puzzle)),
    clientOnlyConfig('PlowTruck'),
    clientOnlyConfig('DeliveryProblem'),
];
